import { combineReducers } from 'redux'
import category from './modules/ecommerce/category'
import cart from './modules/ecommerce/cart'
import status from './modules/status';
import tax from './modules/tax';
import producer from './modules/ecommerce/producer';
import product from './modules/ecommerce/product';
import auth from './modules/auth'
import customer from './modules/customer'

const rootReducer = combineReducers({
    auth,
    cart,
    category,
    producer,
    status,
    tax,
    product,
    customer,
});

export default rootReducer;