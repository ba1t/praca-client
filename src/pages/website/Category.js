import React from 'react';
import {connect} from 'react-redux';
import {actions as productActions, ProductList} from '../../modules/ecommerce/product';
import Loader from '../../shared/Loader'
class Category extends React.Component {
    constructor(props){
        super(props);
        this.state = {}
    }
    componentDidMount() {
        if(!!this.props.match.params.path){
            const path = this.props.match.params.path;
            this.props.fetchProductByCategoryPath(path);
        }
    }
    componentDidUpdate(prevProps) {
        if(this.props.match.params.path !== prevProps.match.params.path) {
            const path = this.props.match.params.path;
            this.props.fetchProductByCategoryPath(path);
        }
    }
    render() {
        return (     
            this.props.match.params.path && this.props.product.list ? <ProductList items={this.props.product.list} /> : <Loader/>
        );
    }
}

const mapStateToProps =  ({product}) => (
    {product}
) 

export default connect(mapStateToProps, { ...productActions })(Category);