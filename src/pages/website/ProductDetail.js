import React from 'react';
import {connect} from 'react-redux';
import {actions} from '../../modules/ecommerce/product';
import {actions as cartActions} from '../../modules/ecommerce/cart';
import {Col, Row, Button} from 'react-bootstrap';
class ProductDetails extends React.Component {
    componentDidMount() {
        this.props.getProduct(this.props.match.params.id);    
    }
    componentWillUnmount(){
        this.props.clearProduct();
    }
    render() {
        console.log(this.props);
        const {details} = this.props.product;
        return (
            details ?

            <Row className="mt-4">
                <Col md={6}>
                    <img className={'img-fluid'} src={`/${details.image[0].target.path}`} alt="" />    
                </Col>
                <Col md={6}>
                    <h2>{details.name}</h2>
                    <p>{details.body}</p>
                    <h3>
                        Cena: {details.price} zł
                    </h3>
                    <Button onClick={() => this.props.addToCart(details._id)}>Dodaj do koszyka</Button>
                </Col>
            </Row> : null
        )
    }
}

const mapStateToProps =  ({product}) => (
    {product}
) 

export default connect(mapStateToProps, { ...actions, ...cartActions })(ProductDetails);

