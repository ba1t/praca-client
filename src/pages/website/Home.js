import React from 'react';
import {connect} from 'react-redux';
import {actions as productActions, ProductList} from '../../modules/ecommerce/product';
import Loader from '../../shared/Loader'
import {Carousel} from 'react-bootstrap';
class Home extends React.Component {
    constructor(props){
        super(props);
        this.state = {}
    }
    componentDidMount() {
        this.props.fetchProduct()
    }
    render() {
        return (     
            <React.Fragment>
                <Carousel>
                    <Carousel.Item>
                        <img
                        className="d-block w-100"
                        alt=""
                        src="https://pixbook.pl/sites/default/files/slider/2_bez_tekstu%202.jpg"
                        />
                    </Carousel.Item>
                </Carousel>
                {this.props.product.list ? <ProductList title="Ostatnio dodane" items={this.props.product.list} limit={6} /> : <Loader/>}
            </React.Fragment>
        );
    }
}

const mapStateToProps =  ({product}) => (
    {product}
) 

export default connect(mapStateToProps, { ...productActions })(Home);

