import React from 'react';
import {connect} from 'react-redux';
import {actions as cartActions, CartForm} from '../../modules/ecommerce/cart';
import {Table, Row, Col} from 'react-bootstrap';
import {Button, ButtonGroup} from 'react-bootstrap'
import Loader from '../../shared/Loader'
// import {Table} from '../../shared/Table'
import {FaMinusSquare, FaPlusSquare} from 'react-icons/fa';
import _ from 'lodash';

class Cart extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            init: false,
            finalPrice: 0
        }
        this.finalPrice = this.finalPrice.bind(this);
    }
    componentDidMount() {
        this.props.getFrontCart(cart => (
            this.setState({init: true})
        ))
    }
    finalPrice() {
        let price = 0;
        for(const el of this.props.cart.front.products) {
            price = price + (el.count * el._id.price);
        }
        return price;
    }
    render() {
            return (
                this.state.init ? 
                this.props.cart.front.products.length ?
                <Row className="mt-4">
                    <Col md={8}>
                        <Table>
                            <thead>
                                <tr>
                                    <td>Nazwa</td>
                                    <td>cena jednostkowa</td>
                                    <td>ilość</td>
                                    <td>cena</td>
                                    <td></td>
                                </tr>
                            </thead>
                            <tbody>
                                {_.map(this.props.cart.front.products, el => {
                                    return (                  
                                        <tr>
                                            <td>{el._id.name}</td>
                                            <td>{el._id.price} zł</td>
                                            <td>{el.count}</td>
                                            <td>{el.count * el._id.price} zł</td>
                                            <td>
                                                <ButtonGroup>
                                                    <Button variant="success" onClick={() => {this.props.addToCart(el._id._id)}}><FaPlusSquare /></Button>
                                                    <Button variant="danger" onClick={() => {console.log('minus')}}><FaMinusSquare /></Button>
                                                    <Button variant="info" onClick={() => (this.props.removeFromCart(el._id._id))}>usuń</Button>
                                                </ButtonGroup>
                                            </td>
                                        </tr>
                                    )
                                })}
                            </tbody>
                        </Table>
                        <h2>
                            Wartość koszyka: {this.finalPrice()}
                        </h2>
                    </Col>
                    <Col md className="cart__from">
                        <CartForm data={this.props.cart.front.products}/>
                    </Col>
                </Row>
                : <div>Twoj koszyk jest pusty</div>
                : <Loader />
            );
        }
    
}

const mapStateToProps =  ({cart}) => (
    {cart}
) 

export default connect(mapStateToProps, { ...cartActions })(Cart);