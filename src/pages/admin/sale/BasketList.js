import React from 'react';
import {connect} from 'react-redux'
import {ThemeContext} from '../../../themes/admin';
import { actions, CartTable } from '../../../modules/ecommerce/cart';

class BasketList extends React.PureComponent {
    static contextType = ThemeContext;
    componentDidMount() {
        this.context.setSubHeader({
            title: "Koszyki klientów",
            link: false
            
        })
        this.props.fetchCarts();
    }
    render() {
        return(
            this.props.cart.list ? 
            <CartTable {...this.props } /> : null
        )
    }
}

const mapStateToProps =  ({cart}) => (
    {cart}
) 

export default connect(mapStateToProps, { ...actions })(BasketList);


