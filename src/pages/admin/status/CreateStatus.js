import React from 'react';

import SettignsForm from '../../../modules/status/form/SettingsForm';
import {ThemeContext} from "../../../themes/admin";

class CreateStatus extends React.PureComponent {
    static contextType = ThemeContext;

    componentDidMount() {
        this.context.setSubHeader({
            title: "Dodaj kategorie",
            link: {
                href: "/admin/ecommerce/categories",
                label: "Powrót do kategorii"
            }
        })
    }
    render(){
        return (
            <SettignsForm {...this.props}/>
        )
    }

};

export default CreateStatus;