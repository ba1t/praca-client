import React from 'react';

import SettignsForm from '../../../modules/status/form/SettingsForm';
import {ThemeContext} from "../../../themes/admin";

class EditStatus extends React.PureComponent{
    static contextType = ThemeContext;

    componentDidMount() {
        this.context.setSubHeader({
            title: `Edytuj status: (${this.props.match.params.id})`,
            link: {
                href: "/category/status",
                label: "Powrót do statusów"
            }
        })
    }
    render() {
        return (
            <SettignsForm {...this.props} edit/>
        )
    }
};

export default EditStatus;