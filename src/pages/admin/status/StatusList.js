import React  from 'react';
import StatusTable from '../../../modules/status/table/StatusTable';
import {ThemeContext} from '../../../themes/admin';
class StatusList extends React.PureComponent{
    static contextType = ThemeContext;
    
    componentDidMount(){
        this.context.setSubHeader({
            title: "Status",
            link: {
                href: "/config/status/add",
                label: "Dodaj status"
            }
        })
    }
    render() {
        return (
            <StatusTable />
        )
    }
};

export default StatusList;

