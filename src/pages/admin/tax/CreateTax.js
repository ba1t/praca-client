import React, { PureComponent} from 'react';
import {connect} from 'react-redux';
import {actions, SettingsForm} from '../../..//modules/tax';
import {ThemeContext} from "../../../themes/admin";

class CreateTax extends PureComponent {
    static contextType = ThemeContext;
    componentWillUnmount() {
        this.props.clearTax()
    }
    componentDidMount(){
        this.context.setSubHeader({
            title: "Dodaj podatek vat",
            link: {
                href: "/admin/config/tax",
                label: "Powrót do podatku vat"
            }
        })
        this.props.fetchTax();
    }
    render(){
        return (
            this.props.tax.list ?
            <SettingsForm {...this.props}/> : null
        )
    }
};

const mapStateToProps =  ({tax}) => (
    {tax}
) 

export default connect(mapStateToProps, { ...actions })(CreateTax);