import React, { PureComponent} from 'react';
import {connect} from 'react-redux';
import {actions, SettingsForm} from '../../..//modules/tax';
import {ThemeContext} from "../../../themes/admin";

class EditTax extends PureComponent {
    static contextType = ThemeContext;
    componentWillUnmount() {
        this.props.clearTax()
    }
    componentDidMount(){  
        this.context.setSubHeader({
            title: `Edytuj podatek vat: (${this.props.match.params.id})`,
            link: {
                href: "/admin/config/tax",
                label: "Powrót do podatku vat"
            }
        })
        this.props.fetchTax();
        this.props.getTax(this.props.match.params.id);
    }
    render(){
        return (
            this.props.tax.list ?
            <SettingsForm {...this.props}/> : null
        )
    }
};

const mapStateToProps =  ({tax}) => (
    {tax}
) 

export default connect(mapStateToProps, { ...actions })(EditTax);