import React, { PureComponent } from 'react';
import { connect } from 'react-redux';

import { actions, TaxTable } from '../../../modules/tax';

import {ThemeContext} from '../../../themes/admin';

class TaxList extends PureComponent  {
    static contextType = ThemeContext;
    
    componentDidMount() {
        this.context.setSubHeader({
            title: "Podatek",
            link: {
                href: "/admin/config/tax/add",
                label: "Dodaj podatek"
            }
        })
        this.props.fetchTax();
    }
    
    render() {
        return(
            this.props.tax.list ? 
            <TaxTable {...this.props } /> : null
        )
    }
}

const mapStateToProps =  ({tax}) => (
    {tax}
) 

export default connect(mapStateToProps, { ...actions })(TaxList);