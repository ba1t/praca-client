import React, { PureComponent} from 'react';
import {connect} from 'react-redux';
import {actions, SettingsForm} from '../../../../modules/ecommerce/category';
import {ThemeContext} from "../../../../themes/admin";

class EditCategory extends PureComponent {
    static contextType = ThemeContext;
    componentWillUnmount() {
        this.props.clearCategory()
    }
    componentDidMount(){
        this.context.setSubHeader({
            title: `Edytuj kategorie: (${this.props.match.params.id})`,
            link: {
                href: "/admin/ecommerce/categories",
                label: "Powrót do kategorii"
            }
        })
        this.props.fetchCategories();
        this.props.getCategory(this.props.match.params.id);
    }
    render(){
        console.log(this.props);
        return (
            this.props.category.list && this.props.category.details ?
            <SettingsForm {...this.props} edit/> : null
        )
    }
};

const mapStateToProps =  ({category}) => (
    {category}
) 

export default connect(mapStateToProps, { ...actions })(EditCategory);