import React, { PureComponent} from 'react';
import {connect} from 'react-redux';
import {actions, SettingsForm} from '../../../../modules/ecommerce/category';
import {ThemeContext} from "../../../../themes/admin";
import SubHeader from '../../../../themes/admin/templates/SubHeader'
class CreateCategory extends PureComponent {
  static contextType = ThemeContext;
  componentWillUnmount() {
    this.props.clearCategory()
  }
  render(){
    return (
      <React.Fragment>
        <SubHeader title={"Dodaj kategorie"} link={{
          href: "/admin/ecommerce/categories",
          label: "Powrót do kategorii"
        }}/>
        <SettingsForm {...this.props}/> 
      </React.Fragment>
    )
  }
};

const mapStateToProps =  ({category}) => (
  {category}
) 

export default connect(mapStateToProps, { ...actions })(CreateCategory);