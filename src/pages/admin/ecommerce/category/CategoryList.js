import React, { PureComponent } from 'react';
import { connect } from 'react-redux';

import { actions, CategoryTable } from '../../../../modules/ecommerce/category';

import {ThemeContext} from '../../../../themes/admin';

class CategoryList extends PureComponent  {
    static contextType = ThemeContext;
    
    componentDidMount() {
        this.context.setSubHeader({
            title: "Kategorie",
            link: {
                href: "/admin/ecommerce/categories/add",
                label: "Dodaj kategorie"
            }
        })
        this.props.fetchCategories();
    }
    
    render() {
        return(
            this.props.category.list ? 
            <CategoryTable {...this.props } /> : null
        )
    }
}

const mapStateToProps =  ({category}) => (
    {category}
) 

export default connect(mapStateToProps, { ...actions })(CategoryList);


