import React, { PureComponent} from 'react';
import {connect} from 'react-redux';
import _ from 'lodash';

import {actions as productActions, SettingsForm} from '../../../../modules/ecommerce/product';
import {actions as producerActions} from '../../../../modules/ecommerce/producer';
import {actions as categoryActions} from '../../../../modules/ecommerce/category';
import {actions as taxActions} from '../../../../modules/tax';
import {actions as statusActions} from '../../../../modules/status';

import {ThemeContext} from "../../../../themes/admin";

class CreateCategory extends PureComponent {
    static contextType = ThemeContext;

    componentDidMount(){
        this.context.setSubHeader({
            title: "Dodaj produkt",
            link: {
                href: "/admin/ecommerce/products",
                label: "Powrót do produktów"
            }
        })
        this.props.fetchCategories();
        this.props.fetchProducer();
        this.props.fetchTax();
    }
    render(){
        return (
            <React.Fragment>          
                {this.props.category.list && this.props.producer.list && this.props.tax.list ? <SettingsForm {...this.props}/> : null}
            </React.Fragment>

        )
    }
};

const mapStateToProps =  ({category, producer, product, tax, status}) => (
    {category, producer, product, tax, status}
) 

const actions = _.merge(productActions, producerActions, taxActions, categoryActions, statusActions);

export default connect(mapStateToProps, { ...actions })(CreateCategory);