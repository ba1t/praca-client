import React, { PureComponent } from 'react';
import { connect } from 'react-redux';

import { actions, ProductTable } from '../../../../modules/ecommerce/product';

import {ThemeContext} from '../../../../themes/admin';

class ProductList extends PureComponent  {
    static contextType = ThemeContext;

    componentDidMount() {
        this.context.setSubHeader({
            title: "Produkty",
            link: {
                href: "/admin/ecommerce/products/add",
                label: "Dodaj produkt"
            }
        })
        this.props.fetchProduct();
    }
    componentWillUnmount() {
        this.props.clearProduct();
    }
    render() {
        return(
            this.props.product.list ? 
            <ProductTable {...this.props } /> : null
        )
    }
}

const mapStateToProps =  ({product}) => (
    {product}
) 

export default connect(mapStateToProps, { ...actions })(ProductList);


