
import React, { PureComponent} from 'react';
import {connect} from 'react-redux';
import {actions, SettingsForm} from '../../../../modules/ecommerce/producer';
import {ThemeContext} from "../../../../themes/admin";

class EditProducer extends PureComponent {
    static contextType = ThemeContext;
    componentWillUnmount() {
        this.props.clearProducer()
    }
    componentDidMount(){
        this.context.setSubHeader({
            title: `Edytuj producenta: (${this.props.match.params.id})`,
            link: {
                href: "/admin/ecommerce/producers",
                label: "Powrót do producentów"
            }
        })
        this.props.fetchProducer();
        this.props.getProducer(this.props.match.params.id);
    }
    render(){
        return (
            this.props.producer.list && this.props.producer.details ?
            <SettingsForm {...this.props} edit/> : null
        )
    }
};

const mapStateToProps =  ({producer}) => (
    {producer}
) 

export default connect(mapStateToProps, { ...actions })(EditProducer);