
import React, { PureComponent} from 'react';
import {connect} from 'react-redux';
import {actions, SettingsForm} from '../../../../modules/ecommerce/producer';
import {ThemeContext} from "../../../../themes/admin";

class CreateProducer extends PureComponent {
    static contextType = ThemeContext;
    componentWillUnmount() {
        this.props.clearCategory()
    }
    componentDidMount(){
        this.context.setSubHeader({
            title: "Dodaj producenta",
            link: {
                href: "/admin/ecommerce/producers",
                label: "Powrót do producentów"
            }
        })
        this.props.fetchProducer();
    }
    render(){
        return (
            this.props.producer.list ?
            <SettingsForm {...this.props}/> : null
        )
    }
};

const mapStateToProps =  ({producer}) => (
    {producer}
) 

export default connect(mapStateToProps, { ...actions })(CreateProducer);