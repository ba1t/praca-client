import React, { PureComponent } from 'react';
import { connect } from 'react-redux';

import { actions, ProducerTable } from '../../../../modules/ecommerce/producer';

import {ThemeContext} from '../../../../themes/admin';

class ProducerList extends PureComponent  {
    static contextType = ThemeContext;
    
    componentDidMount() {
        this.context.setSubHeader({
            title: "Producenci",
            link: {
                href: "/admin/ecommerce/producers/add",
                label: "Dodaj producenta"
            }
        })
        this.props.fetchProducer();
    }
    
    render() {
        return(
            this.props.producer.list ? 
            <ProducerTable {...this.props } /> : null
        )
    }
}

const mapStateToProps =  ({producer}) => (
    {producer}
) 

export default connect(mapStateToProps, { ...actions })(ProducerList);

