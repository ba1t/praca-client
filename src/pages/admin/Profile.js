import React from 'react';
import {connect} from 'react-redux';
import { Redirect } from 'react-router-dom';
import {Alert } from 'react-bootstrap';
import {actions} from '../../modules/auth';
import Form from "react-jsonschema-form";
import axios from 'axios';
class Profile extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            redirectToReferrer: false,
            profile: false,
            alert: false,
            id: false
        }
        this.onSubmit = this.onSubmit.bind(this);
    }
    async componentDidMount() {
        const res = await axios.get(`/api/profile`);
        this.setState({profile: res.data.data.profile, id: res.data.data._id})
    }
    async onSubmit({formData}){
        try {
            const res = await axios.post(`/api/user/${this.state.id}`, {...formData});
            console.log(formData);
            if(res.data.success) {
                this.setState({alert: res.data.msg})
            } else {
                this.setState({alert: res.data.msg})
            }
        } catch(e) {
            console.log(e)
        }

    }

    uiSchema = {
        "password": {
            "ui:widget": "password",
            "ui:title": "Your password"
        }
    }
    schema = {
        "type": "object",
        "properties": {
            "firstname": {
                "type": "string",
                "title": "Imie",
            },
            "lastname": {
                "type": "string",
                "title": "Nazwisko",
            },
            "phone": {
                "type": "string",
                "title": "telefon",
            },
            "address1": {
                "type": "string",
                "title": "Address1",
            },
            "address2": {
                "type": "string",
                "title": "Address2",
            }
        }

    }
    
    renderForm() {
        return this.props.auth && this.props.auth.loggedIn ?
        <div>
            {this.state.alert && <Alert  variant="info" onClose={() => this.setState({alert: false})} dismissible>
                <p>
                    {this.state.alert}
                </p>
            </Alert>

            }
            <Form schema={this.schema} uiSchema={this.uiSchema} onSubmit={this.onSubmit} formData={this.state.profile}/>
        </div>
        :  <Redirect to={{pathname: '/'}} />
    }
    render() {
        return (
            this.renderForm()
        );
    }
}

const mapStateToProps =  ({auth}) => (
    {auth}
) 

export default connect(mapStateToProps, { ...actions })(Profile);