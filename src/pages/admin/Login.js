import React from 'react';
import {connect} from 'react-redux';
import { Redirect } from 'react-router-dom';
import {Alert } from 'react-bootstrap';
import {actions} from '../../modules/auth';
import Form from "react-jsonschema-form";
import axios from 'axios';
class Login extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            redirectToReferrer: false,
            alert: false
        }
        this.onSubmit = this.onSubmit.bind(this);
    }
    async onSubmit({formData}){
        console.log('xxx');
        try {
            const res = await axios.post(`/api/login`, formData);
            if(res.data.success) {
                console.log(res.data);
                this.props.authUser(() => (
                    this.setState({redirectToReferrer: true})
                ))
            } else {
                this.setState({alert: res.data.msg})
            }
        } catch(e) {
            console.log(e)
        }

    }

    uiSchema = {
        "password": {
            "ui:widget": "password",
            "ui:title": "Your password"
        }
    }
    schema = {
        "type": "object",
        "required": [
            "email",
            "password"
        ],
        "properties": {
            "email": {
                "type": "string",
                "title": "Email",
            },
            "password": {
                "title": "password",
                "type": "string",
            }
        }
    }
    componentDidMount() {
        this.props.auth && this.props.auth.loggedIn && this.setState({redirectToReferrer: true})
    }
    
    renderForm() {
        return this.props.auth && this.props.auth.loggedIn  ?
        <Redirect to={{pathname: '/admin'}} />
        : 
        <div>
            {this.state.alert && <Alert  variant="danger" onClose={() => this.setState({alert: false})} dismissible>
                <p>
                    {this.state.alert}
                </p>
            </Alert>
            }
                  
            <Form schema={this.schema} uiSchema={this.uiSchema} onSubmit={this.onSubmit} />

        </div>

    }
    render() {
        return (
            this.state.redirectToReferrer ? 
            <Redirect to={{pathname: '/admin'}} />
            : this.renderForm()
        );
    }
}

const mapStateToProps =  ({auth}) => (
    {auth}
) 

export default connect(mapStateToProps, { ...actions })(Login);