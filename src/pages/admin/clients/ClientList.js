import React, { PureComponent } from 'react';
import { connect } from 'react-redux';

import { actions, CustomerTable } from '../../../modules/customer';

import {ThemeContext} from '../../../themes/admin';

class CustomerList extends PureComponent  {
    static contextType = ThemeContext;
    
    componentDidMount() {
        this.context.setSubHeader({
            title: "Klienci",
        })
        this.props.fetchCustomers();
    }
    
    render() {
        return(
            this.props.customer.list ? 
            <CustomerTable {...this.props } /> : null
        )
    }
}

const mapStateToProps =  ({customer}) => (
    {customer}
) 

export default connect(mapStateToProps, { ...actions })(CustomerList);


