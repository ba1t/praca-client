import React from 'react';
import {connect} from 'react-redux';
import {actions} from './modules/auth';
import Router from './Router';
 
class App extends React.Component {
  componentDidMount() {
    this.props.authUser()
  }
  render(){
    return (
      this.props.auth ?
        <Router auth={this.props.auth} />
        : null
    )
  }
}
const mapStateToProps =  ({auth}) => (
  {auth}
) 
 
export default connect(mapStateToProps, { ...actions })(App);