import React, { Suspense } from 'react';
import { ThemeProvider } from '../index';
import Header from './Header';
import {Container} from 'react-bootstrap';
import Loader from '../../../shared/Loader'
import '../scss/style.scss'
export default (Component, auth) => {
    const Pages = (props) => {
        return (
            <ThemeProvider>
                <div className={`client-page container`}>
                    <nav className="client-nav container">
                        <Header auth={auth}/>
                    </nav>
                    <main className="client-content">
                        <div className="content">      
                            <Suspense fallback={<Loader />}>
                                <div className="content__inner">
                                    <Container>
                                        <Component {...props}/>
                                    </Container>
                                </div>
                            </Suspense>
                        </div>
                    </main>
                </div>
            </ThemeProvider>
        )
    }
    return Pages;
}