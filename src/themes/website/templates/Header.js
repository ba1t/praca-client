import React from 'react';
import {Navbar, Nav, NavDropdown} from 'react-bootstrap';
import {CartWidget} from '../../../modules/ecommerce/cart';
import { Link } from 'react-router-dom';
import {CategoryNavMenu} from '../../../modules/ecommerce/category'
import {isAdmin} from '../../../lib/helpers';
import {LoginBlock} from '../../../modules/auth';

const Header = (props) => (
    <Navbar bg="primary" variant="dark">
        <Navbar.Brand as={Link} to="/">Store</Navbar.Brand>
        <Nav className="mr-auto">
            <NavDropdown title="Kategorie" id="basic-nav-dropdown">
                <CategoryNavMenu />
            </NavDropdown>
            { isAdmin(props.auth) && 
                <Nav.Item>
                    <Nav.Link as={Link} to="/admin">Admin panel</Nav.Link>
                </Nav.Item>
            }
        </Nav>
        <LoginBlock {...props}/>

        <CartWidget />
    </Navbar>
)

export default Header;