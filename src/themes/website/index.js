import Page from './templates/Page';
import 'react-perfect-scrollbar/dist/css/styles.css';
import React, {createContext} from 'react';

export const ThemeContext = createContext()

export const ThemeProvider = (props) => {
    return (
        <ThemeContext.Provider>
            {props.children}
        </ThemeContext.Provider>

    )

};
    
export const ThemeConsumer = ThemeContext.Consumer;

export default (Component, auth) => (
    Page(Component, auth)
)