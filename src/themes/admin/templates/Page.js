import React, { Suspense } from 'react';
import Header from './Header';
import SubHeader from './SubHeader';

import Loader from '../../../shared/Loader'

import Menu from './Menu'
import {adminMenu} from '../../../config/menu'
import '../scss/style.scss'

export default (Component, auth) => {
    const Pages = (props) => {
        return (
            <div className={`app-page`}>
                <aside className="app-sidebar">
                    <div className="app-sidebar__header">
                        <h2 className="">Panel</h2>
                    </div>
                    <Menu path={props.match.path} data={adminMenu} />
                </aside>
                <nav className="app-nav">
                    <Header {...props} auth={auth}/>
                    <SubHeader/>
                </nav>
                <main className="app-content">
                    {/* <PerfectScrollbar> */}
                        <div className="content">      
                            <Suspense fallback={<Loader />}>
                                <div className="content__inner">
                                    <Component {...props}/>
                                </div>
                            </Suspense>
                        </div>

                    {/* </PerfectScrollbar> */}
                </main>
            </div>
        )
    }

    return Pages;

}