import React from 'react';
import { Link } from 'react-router-dom';
import {FaChevronRight, FaChevronDown} from 'react-icons/fa';
import { FaCircle, FaEllipsisV } from 'react-icons/fa'
import PerfectScrollbar from 'react-perfect-scrollbar'
import _ from 'lodash';
class Menu extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            open: false
        }
        this.openSubemnu = this.openSubemnu.bind(this);
        this.checkSubmenuActive = this.checkSubmenuActive.bind(this);
    }
    componentDidMount() {
        this.props.data.map(item => (
            this.checkSubmenuActive(item.id, item.submenu)
        ))
    }
    openSubemnu(id) {
        if(id !== this.state.open) {
            this.setState({open: id});
        } else {
            this.setState({open: false});
        }
    }
    checkSubmenuActive (id, submenu) {
        if(!!_.find(submenu, {href: this.props.path})){
           return this.setState({open: id});
        }
    }
    render() {
        return (
            <PerfectScrollbar>
                <ul className={this.props.className}>
                    {this.props.data.map(item => {
                        return (
                            <React.Fragment key={item.path}>
                                {item.section && <li className="menu__section"><h4 className="menu__section--text">{item.section}</h4></li>}
                                <li className={`menu__item ${this.props.path === item.href  ? 'menu__item--active' : ''}`} key={item.label}>
                                    <Link onClick={()=> item.submenu ? this.openSubemnu(item.id) : true } className={`menu__link ${this.state.open === item.id ? "menu__link--open" : ""}`} to={item.href}>
                                        <span className="menu__link-item">
                                            {item.icon ? <i className="menu__link-icon"><item.icon /></i> : <i className="menu__link-icon menu__link-icon--dot"><FaCircle /></i>}
                                            <span className="menu__link-text">{item.label}</span>
                                        </span>
                                        {item.submenu ? (this.state.open === item.id ? <FaChevronDown /> : <FaChevronRight />) : <FaEllipsisV /> }
                                    </Link>
                                    {
                                        (item.submenu && this.state.open === item.id) &&
                                        <div className={`menu__submenu`}>
                                            <Menu path={this.props.path} className="menu__subnav" active={this.props.active} data={item.submenu} />
                                        </div>
                                    }
                                </li>
                            </React.Fragment>
                        )
                    })}
                </ul>
            </PerfectScrollbar>
        )
    }
}

Menu.defaultProps = {
    data: [],
    className: "menu__nav"
}

export default Menu;