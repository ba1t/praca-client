import React from 'react';
import {Navbar, NavbarSection} from '../../../shared/Navbar';
import Button from '../../../shared/Button/Button';

const SubHeader = (props) => (
    <Navbar justyfi={'space-between'}>
        <NavbarSection>
            <h2>#{props.title}</h2>
        </NavbarSection>
        <NavbarSection>
            {props.link && <Button color="success" to={props.link.href}>{props.link.label || "Dodaj nowy"}</Button>}
        </NavbarSection>
    </Navbar>
)

export default SubHeader;