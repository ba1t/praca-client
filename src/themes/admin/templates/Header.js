import React from 'react';
import {Navbar, NavbarSection} from '../../../shared/Navbar';
import { Link } from 'react-router-dom';
import Button from '../../../shared/Button/Button';
import {connect} from 'react-redux';
import {actions} from '../../../modules/auth';

class Header extends React.Component {
    render(){
    return (
            <Navbar className="header" justyfi={'space-between'}>
                <NavbarSection className="header__sites">
                    <Link to='/'><Button color={'secoundary'}>Strona</Button></Link>
                    <Button color={'secoundary'}>Docs</Button>
                </NavbarSection>
                <NavbarSection className="header__user">
                    witaj, {this.props.auth.user.email}, <Button onClick={() => this.props.logoutUser(()=>  this.props.history.push("/"))} color={'primary'}>Wyloguj</Button>
                </NavbarSection>
            </Navbar>
        )
    }
}
const mapStateToProps =  ({auth}) => (
    {auth}
) 

export default connect(mapStateToProps, { ...actions })(Header);