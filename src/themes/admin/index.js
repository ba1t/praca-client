import Page from './templates/Page';
import 'react-perfect-scrollbar/dist/css/styles.css';

export default (Component, auth) => (
    Page(Component, auth)
)