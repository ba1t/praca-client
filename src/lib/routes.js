import React from 'react';
import { Redirect, Route } from 'react-router-dom';
import {isAdmin} from './helpers';

export const renderPrivateRoutes = ({component: Component, auth, ...rest}) => (
    <Route {...rest} render={props => {
        if(!!auth ) {
            return <Component {...props} />
        } else {
            return (
                <Redirect to={
                    {
                        path: '/login',
                        state: {from: props.location}
                    }
            } />)
        }
    }} />
);

export const renderRoutes = (config, theme, auth, alias = "") => (
    config.map((route) => {
        if(route.private) {
            console.log(isAdmin(auth))
            if(isAdmin(auth)) {
                return  (
                    <Route 
                        exact={route.exact} 
                        key={route.path + "route"} 
                        path={alias + route.path} 
                        component={theme(route.component, auth)} 
                    />
                )
            } else {
                return (
                    <Redirect to={{ path: '/login' }} />
                )
            }
        } else {
            return(
                <Route 
                    exact={route.exact} 
                    key={route.path + "route"} 
                    path={alias + route.path} 
                    component={theme(route.component, auth)} 
                />
            )
        }
    })
)