
export const arrayToObject = (array, key) => {
    console.log(array)
    return (
        array.reduce((obj, item) => {
            obj[item[key]] = item[key];
            return obj;
        }, {})
    )
}

export const isAdmin = (auth) => {
    return auth && auth.loggedIn && auth.user && auth.user.role === 'admin';
}