import axios from "axios";
export const fileUpload = (file, callback) => {
    const formData = new FormData();
    file.forEach(item =>  {
        console.log(item);
        formData.append('images', item);
    })
    console.log(formData);
    axios.post('/api/upload', formData).then(response => {
        callback(response.data);
    }).catch(error => {
        console.log("*****  "+error)
    })
}