import axios from 'axios';

export default {
    fetchTax() {
        return axios.get('/api/v1/tax')
    },
    getTax(id) {
        return axios.get(`/api/v1/tax/${id}`);
    },
    addTax(data) {
        return axios.post(`/api/v1/tax`, data);
    },
    updateTax(id, data) {
        return axios.put(`/api/v1/tax/${id}`, data);
    },
    deleteTax(id) {
        return axios.delete(`/api/v1/tax/${id}`);
    }
}