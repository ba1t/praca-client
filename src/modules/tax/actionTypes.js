export const FETCH_TAX = '@@store/ecoomerce/tax/FETCH_TAX'; 
export const GET_TAX = '@@store/ecoomerce/tax/GET_TAX';
export const ADD_TAX = '@@store/ecoomerce/tax/ADD_TAX';
export const UPDATE_TAX = '@@store/ecoomerce/tax/UPDATE_TAX';
export const DELETE_TAX = '@@store/ecoomerce/tax/DELETE_TAX';
export const CLEAR_TAX = '@@store/ecoomerce/tax/CLEAR_TAX';