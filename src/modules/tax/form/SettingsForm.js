import React from 'react';
import {withRouter} from 'react-router-dom';
import Form from "react-jsonschema-form";
class SettingsForm extends React.Component {
    constructor(props){
        super(props);
        this.onSubmit = this.onSubmit.bind(this);
    }
    schema = {
        "type": "object",
        "required": [
            "name",
            "rate"
        ],
        "properties": {
            "name": {
                "type": "string",
                "title": "Nazwa",
            },
            "rate": {
                "type": 'string',
                "title": 'Rate',
            }
        }
    }
    onSubmit({formData}){
        this.props.edit ?
            this.props.updateTax(this.props.match.params.id, formData, () => ( this.props.history.push("/admin/config/tax") ))
            : this.props.addtax(formData, () => ( this.props.history.push("/admin/config/tax") ));
        }
    
    render() {
        return (
            this.props.tax.list ? 
            <Form schema={this.schema} onSubmit={this.onSubmit} formData={this.props.tax.details} /> : null
        )
    }
}

export default withRouter(SettingsForm);