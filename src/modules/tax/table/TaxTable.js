import React from 'react';
import { connect } from 'react-redux';
import {actions } from '..';

import {Table} from '../../../shared/Table'
import Button from '../../../shared/Button/Button'
import ButtonGroup from '../../../shared/ButtonsGroup/ButtonGroup';
import { FaTrash } from 'react-icons/fa';

class TaxTable extends React.Component {
    constructor(props) {
        super(props);
        this.action = this.action.bind(this);
    }
    columns = [
        { head: 'Name', accessor: 'name'},      
        { head: 'Rate', accessor: 'rate'},      
    ];
    componentDidMount() {
        this.props.fetchTax();
    }
    action(arg) {
        return (
            <ButtonGroup>
                <Button to={`/admin/config/tax/${arg.id}/edit`} color="secoundary">Edytuj</Button>
                <Button color="danger" onClick={() => (this.props.deleteTax(arg.id))}><FaTrash /></Button>
            </ButtonGroup>
        )
    }
    render() {
        return (
            this.props.tax.list ? <Table columns={this.columns} actions={this.action} rows={this.props.tax.list} /> : null       
        )
    }
}

const mapStateToProps =  ({tax}) => (
    {tax}
) 

export default connect(mapStateToProps, { ...actions })(TaxTable);