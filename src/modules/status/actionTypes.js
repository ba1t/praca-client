export const FETCH_STATUS = '@@store/ecoomerce/status/FETCH_STATUS'; 
export const GET_STATUS = '@@store/ecoomerce/status/GET_STATUS';
export const ADD_STATUS = '@@store/ecoomerce/status/ADD_STATUS';
export const UPDATE_STATUS = '@@store/ecoomerce/status/UPDATE_STATUS';
export const DELETE_STATUS = '@@store/ecoomerce/status/DELETE_STATUS';
export const CLEAR_STATUS = '@@store/ecoomerce/status/CLEAR_STATUS';