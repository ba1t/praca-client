import * as Types from "./actionTypes";
import _ from 'lodash';

export default function(state = {}, action) {
    switch (action.type) {
        case Types.FETCH_STATUS:
            return { ...state, ...action.payload }
        case Types.GET_STATUS:
            return {...state, details: action.payload}
        case Types.ADD_STATUS:
            return {...state,newOrder: action.payload}
        case Types.UPDATE_STATUS:
            return {
                ...state,
                updateStatus: action.payload.success,
                status: action.payload.doc
            }
        case Types.DELETE_STATUS:
            const newState = [...state.list];
            const deleteItem = _.findIndex(newState, ['_id', action.payload]);
            newState.splice(deleteItem, 1);
            return {
                ...state,
                count: state.count - 1,
                list: newState,
                statusDeleted:action.payload
            }
        case Types.CLEAR_STATUS:
            return {
                ...state,
                updateStatus: action.payload.updateStatus,
                details: action.payload.details,
                statusDeleted: action.payload.statusDeleted
            }
        default:
            return state;
    }
}