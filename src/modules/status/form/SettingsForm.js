import React from 'react';
import { connect } from 'react-redux';
import {withRouter} from 'react-router-dom';
import { actions } from '../';
import Form from '../../../shared/Form';

class SettingsForm extends React.Component {
    constructor(props){
        super(props);
        this.onSubmit = this.onSubmit.bind(this);
    }
    componentDidMount() {
        this.props.fetchStatus();
        if(this.props.edit) {
            this.props.getStatus(this.props.match.params.id);
        } 
    }
    componentWillUnmount() {
        this.props.clearStatus();
    }
    formBuilder(){
        return ([
            {
                type: 'input',
                label: 'Nazwa',
                name: 'name',
                require: true,
            },
            {
                type: 'select',
                label: 'Typ',
                name: 'type',
                default: 'product',
                options: [
                    {value: 'product', name: 'Produkt'},
                    {value: 'order', name: 'Zamówienie'}
                ]
            }
        ])
    }
    onSubmit(formValues, e){
        e.preventDefault();
        if(this.props.edit) {
            this.props.updateStatus(this.props.match.params.id, formValues, () => (
                this.props.history.push("/config/status")
            ));
        } else {
            this.props.addStatus(formValues, () => (
                this.props.history.push("/config/status")
            ));
        }
    }
    render() {
        return (
            this.props.status.list ? 
            <Form onSubmit={this.onSubmit} edit={this.props.edit} data={this.props.status.details} config={this.formBuilder()} /> : null
        )
    }
}

const mapStateToProps = ({status}) => (
    {status}
)

export default connect(mapStateToProps, {...actions})(withRouter(SettingsForm));