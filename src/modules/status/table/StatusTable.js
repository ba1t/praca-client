import React from 'react';
import { connect } from 'react-redux';
import {actions } from '..';

import {Table} from '../../../shared/Table'
import Button from '../../../shared/Button/Button'
import ButtonGroup from '../../../shared/ButtonsGroup/ButtonGroup';
import { FaTrash } from 'react-icons/fa';

class StatusTable extends React.Component {
    constructor(props) {
        super(props);
        this.action = this.action.bind(this);
    }
    columns = [
        { head: 'Name', accessor: 'name'},      
        { head: 'Typ', accessor: 'type'},      
    ];
    componentDidMount() {
        this.props.fetchStatus();
    }
    action(arg) {
        return (
            <ButtonGroup>
                <Button to={`/config/status/${arg.id}/edit`} color="secoundary">Edytuj</Button>
                <Button color="danger" onClick={() => (this.props.deleteStatus(arg.id))}><FaTrash /></Button>
            </ButtonGroup>
        )
    }
    render() {
        return (
            this.props.status.list ? <Table columns={this.columns} actions={this.action} rows={this.props.status.list} /> : null       
        )
    }
}

const mapStateToProps =  ({status}) => (
    {status}
) 

export default connect(mapStateToProps, { ...actions })(StatusTable);