import axios from 'axios';

export default {
    fetchStatus() {
        return axios.get('/api/status')
    },
    getStatus(id) {
        return axios.get(`/api/status/${id}`);
    },
    addStatus(data) {
        return axios.post(`/api/status`, data);
    },
    updateStatus(id, data) {
        return axios.put(`/api/status/${id}`, data);
    },
    deleteStatus(id) {
        return axios.delete(`/api/status/${id}`);
    }
}