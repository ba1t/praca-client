import * as Types from "./actionTypes";
import services from './services'
export const fetchStatus = () => async dispatch => {
    const res = await services.fetchStatus();
    dispatch({type: Types.FETCH_STATUS, payload: {list: res.data}});
}
// export const getStatusByPath = (path) => async dispatch => {
//     const res = await axios.get(`/api/status/path/${path}`);
//     dispatch({type: Types.GET_STATUS, payload: res.data.doc})
// }
export const getStatus = (id, cb) => async dispatch => {
    const res = await services.getStatus(id);
    dispatch({type: Types.GET_STATUS, payload: res.data})
}
export const addStatus = (data, cb) => async dispatch => {
    const res = await services.addStatus(data);
    dispatch({type: Types.ADD_STATUS, payload: res.data.doc});
    cb();
}
export const updateStatus = (id,data, cb) => async dispatch => {
    const res = await  services.updateStatus(id, data);
    dispatch({type: Types.UPDATE_STATUS, payload: res})
    cb();
}
export const deleteStatus = (id) => dispatch => {
    services.deleteStatus(id);
    dispatch({type: Types.DELETE_STATUS, payload: id})
}
export const clearStatus = () => dispatch => {
    dispatch({type: Types.CLEAR_STATUS, payload: {details: false, updateStatus: false, statusDeleted: false}})
}