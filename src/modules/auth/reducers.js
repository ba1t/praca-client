import * as Types from "./actionTypes";

export default function(state = false, action) {
    switch (action.type) {
        case Types.AUTH_USER:
            return action.payload 
        case Types.LOGOUT_USER:
            return null
        default:
            return state;
    }
}