import React from 'react';
import {Nav, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import {actions} from '../';
const LoginBlock = (props) => {
    return (
        props.auth && props.auth.user ?
        <Nav>
            <Nav.Item>
                <Nav.Link eventKey="disabled" disabled>
                    witaj, {props.auth.user.email}
                </Nav.Link>
            </Nav.Item>
            <Nav.Item>
                <Nav.Link as={Link} to="/profile">Profile</Nav.Link>
            </Nav.Item>
            <Nav.Item>
                <Button onClick={() => props.logoutUser()} color={'primary'}>Wyloguj</Button>
            </Nav.Item>
        </Nav>
        : 
        <Nav>
            <Nav.Item>
                <Nav.Link as={Link} to="/login">Login</Nav.Link>
            </Nav.Item>
            <Nav.Item>
                <Nav.Link as={Link} to="/register">Register</Nav.Link>
            </Nav.Item>
        </Nav>
    )
}

const mapStateToProps =  ({auth}) => (
    {auth}
) 
export default connect(mapStateToProps,{ ...actions })(LoginBlock);