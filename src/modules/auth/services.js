import axios from 'axios';

export default {
    authUser() {
        return  axios.get('/api/auth')
    },
    logoutUser() {
        return axios.get('/api/logout');
    }
}   