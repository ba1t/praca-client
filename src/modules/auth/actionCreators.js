import * as Types from "./actionTypes";
import services from './services'

export const authUser = (cb = false) => async dispatch => {
    const res = await services.authUser();
    dispatch({type: Types.AUTH_USER, payload: res.data})
    if(cb) {
        cb();
    }
}

export const logoutUser = (cb = false) => async (dispatch) => {
    await services.logoutUser();
    dispatch({type: Types.LOGOUT_USER, payload: false})
    if(cb) {
        cb();
    }
}