import React from 'react';

import {Table} from '../../../../shared/Table'
import Button from '../../../../shared/Button/Button'
import ButtonGroup from '../../../../shared/ButtonsGroup/ButtonGroup';
import { FaTrash } from 'react-icons/fa';

export default (props) => {
    const columns = [
        { head: 'Publikacja', accessor: 'published'},       
        { head: 'Name', accessor: 'name', width: "25%" },       
        { head: 'Price', accessor: 'price', width: "25%" },       
        { head: 'Status', accessor: 'status', width: "25%" },       
        { head: 'Kategoria', accessor: 'category.name', width: "25%" },       
        { head: 'Stock', accessor: 'stock'},
    ];

    function actions(arg) {
        return (
            <ButtonGroup>
                <Button to={`/admin/ecommerce/products/${arg.id}/edit`} color="secoundary">Edytuj</Button>
                <Button color="danger" onClick={() => (props.deleteProduct(arg.id))}><FaTrash /></Button>
            </ButtonGroup>
        )
    }
    return (
        <Table columns={columns} actions={actions} rows={props.product.list} />      
    )
}
