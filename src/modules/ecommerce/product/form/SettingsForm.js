import React from 'react';
import {withRouter} from 'react-router-dom';
import Form from "react-jsonschema-form";
import {schema} from '../../../../shared/ImageUploader';

class SettingsForm extends React.Component {
    constructor(props){
        super(props);
        this.onSubmit = this.onSubmit.bind(this);
    }
    componentDidMount() {
        console.log('details', this.props.product.details)
    }
    schema = {
        "type": "object",
        "required": [
            "name",
        ],
        "properties": {
            "name": {
                "type": "string",
                "title": "Produktu",
            },
            'body': {
                "type": "string",
                "title": "Opis"
            },
            "enable": {
                "type": "boolean",
                "title": "Enable",
                "default": true
            },
            "status": {
                "type": 'string',
                "title": 'Status',
                "enum": [
                    'enable',
                    'disable'
                ],
                "enumNames": [
                    'Dostępny',
                    'Nie dostępny'
                ] 
            },
            "category": {
                "title": "Kategoria",
                "type": "string",
                "enum": this.props.category.list.map(ele => (ele._id)),
                "enumNames": this.props.category.list.map(ele => (ele.name ))
            },
            "manufacturer": {
                "title": "Producent",
                "type": "string",
                "enum": this.props.producer.list.map(ele => (ele._id)),
                "enumNames": this.props.producer.list.map(ele => (ele.name ))
            },
            "tax": {
                "title": "Podatek",
                "type": "string",
                "enum": this.props.tax.list.map(ele => (ele._id)),
                "enumNames": this.props.tax.list.map(ele => (ele.name ))
            },
            "stock": {
                "title": "Na magazynie",
                "type": "number"
            },
            "price": {
                "title": "Cena",
                "type": "string"
            },
            "image": {
                "title": "images",
                "type": 'array',
                'items': {
                    "type": "object",
                    "properties": {
                        'target': {
                            "type": "string",
                        },
                        'attributes': {
                            "type": "object",
                            "properties": {
                                'alt': {
                                    "title": "ALT",
                                    "type": "string"
                                },
                                'title': {
                                    "title": "title",
                                    "type": "string"
                                }
                            }
                        }
                    }
                }

            }
        }
    }
    uiSchema = {
        body: {
            "ui:widget": "textarea",
        },
        image: {
            items: {
                target: {
                    "ui:widget": schema
                }
            }
        }
    }
    onSubmit({formData}){
        this.props.product.details ?
            this.props.updateProduct(this.props.match.params.id, formData, () => ( this.props.history.push("/admin/ecommerce/products") ))
            : this.props.addProduct(formData, () => ( this.props.history.push("/admin/ecommerce/products") ));

        }
    
    render() {
        return (
        this.props.category.list && this.props.producer.list && this.props.tax.list ? 
            <Form onSubmit={this.onSubmit} uiSchema={this.uiSchema} schema={this.schema} formData={this.props.product.details} /> : null
        ) 
    }
}

export default withRouter(SettingsForm);