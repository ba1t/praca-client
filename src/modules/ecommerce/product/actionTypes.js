export const FETCH_PRODUCT = '@@store/ecoomerce/product/FETCH_PRODUCT'; 
export const GET_PRODUCT = '@@store/ecoomerce/product/GET_PRODUCT';
export const ADD_PRODUCT = '@@store/ecoomerce/product/ADD_PRODUCT';
export const UPDATE_PRODUCT = '@@store/ecoomerce/product/UPDATE_PRODUCT';
export const DELETE_PRODUCT = '@@store/ecoomerce/product/DELETE_PRODUCT';
export const CLEAR_PRODUCT = '@@store/ecoomerce/product/CLEAR_PRODUCT';