import axios from 'axios';

export default {
    fetchProduct() {
        return axios.get('/api/v1/product')
    },
    fetchProductByCategoryPath(path) {
        return axios.get(`/api/v1/product/category/${path}`);
    },
    getProduct(id) {
        return axios.get(`/api/v1/product/${id}`);
    },
    addProduct(data) {
        return axios.post(`/api/v1/product`, data);
    },
    updateProduct(id, data) {
        return axios.put(`/api/v1/product/${id}`, data);
    },
    deleteProduct(id) {
        return axios.delete(`/api/v1/product/${id}`);
    }
}