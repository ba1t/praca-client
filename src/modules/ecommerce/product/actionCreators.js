import * as Types from "./actionTypes";
import services from './services'
export const fetchProduct = () => async dispatch => {
    const res = await services.fetchProduct();
    dispatch({type: Types.FETCH_PRODUCT, payload: {list: res.data.doc}});
};

export const fetchProductByCategoryPath = (path) => async dispatch => {
    const res = await services.fetchProductByCategoryPath(path);
    dispatch({type: Types.FETCH_PRODUCT, payload: {list: res.data.doc}});
};

export const getProduct = (id, cb) => async dispatch => {
    const res = await services.getProduct(id);
    dispatch({type: Types.GET_PRODUCT, payload: res.data.doc})
};
export const addProduct = (data, cb) => async dispatch => {
    const res = await services.addProduct(data);
    cb(res);
    dispatch({type: Types.ADD_PRODUCT, payload: res.data.doc});
};
export const updateProduct = (id,data, cb) => async dispatch => {
    const res = await  services.updateProduct(id, data);
    dispatch({type: Types.UPDATE_PRODUCT, payload: res});
    cb();
};
export const deleteProduct = (id) => dispatch => {
    services.deleteProduct(id);
    dispatch({type: Types.DELETE_PRODUCT, payload: id});
};
export const clearProduct = () => dispatch => {
    dispatch({type: Types.CLEAR_PRODUCT, payload: {details: false, updateProduct: false, productDeleted: false}})
}