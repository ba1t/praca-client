import React from 'react';
import _ from 'lodash';
import {Button, Row, Card, Col, Modal, ButtonGroup} from 'react-bootstrap';
import {connect} from 'react-redux';
import {actions as cartActions} from '../../cart';
import { Link } from 'react-router-dom';

const ProductItem = (props) => {
    const [modalIsOpen,setIsOpen] = React.useState(false);

    function openModal() {
      setIsOpen(true);
    }
    function closeModal(){
      setIsOpen(false);
    }
    return(
        <React.Fragment>
            <Col sm={4} className="mt-4">
                <Card className="h-100 d-flex justify-content-end">
                    {props.data.image.length && <Card.Img variant="top" src={`/${props.data.image[0].target.path}`} />}
                    <Card.Body>
                        <Card.Title><h2 className="text-center">{props.data.name}</h2></Card.Title>
                        <Row>
                           <Col>Producent:<h3>{props.data.manufacturer && props.data.manufacturer.name}</h3></Col>
                           <Col><div className="text-right">Cena: </div><h3 className="product-item__price text-right">{props.data.price && props.data.price} zł</h3></Col>
                        </Row>
                        <Card.Text className="text-left">
                        {props.data.body && props.data.body.slice(0, 200) + "..."}
                        </Card.Text>
                        <ButtonGroup className="w-100">
                            <Button variant="secondary" className="w-100" as={Link} to={`/products/${props.data._id}`}>Szczegóły</Button>
                            <Button variant="primary" className="w-100" onClick={() => {props.addToCart(props.data._id); openModal();}}>Dodaj do koszyka</Button>
                        </ButtonGroup>
                    </Card.Body>
                </Card>
            </Col>

            <Modal show={modalIsOpen} onHide={closeModal}>
                <Modal.Header closeButton>
                    <Modal.Title>{props.data.name}</Modal.Title>
                </Modal.Header>
                <Modal.Body>Produkt dodany do koszyka</Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" as={Link} to={`/koszyk/${props.cartId}`}>
                        Do koszyka
                    </Button>
                    <Button variant="primary" onClick={closeModal}>
                        Kontynuuj zakupy
                    </Button>
                </Modal.Footer>
            </Modal>
        </React.Fragment>
    )
}


class ProductList extends React.Component {
    constructor(props) {
        super(props);
        this.addToCart = this.addToCart.bind(this);
    }
    addToCart(id) {
        this.props.addToCart(id);
    }
    render() {
        return (
            <React.Fragment>
                <h1 className="display-4 text-center mt-5 mb-4">{this.props.title}</h1>
            {this.props.items && this.props.cart.front && this.props.cart.front._id ?
            <Row>
                {_.map(this.props.limit ? this.props.items.slice(0, this.props.limit) : this.props.items, item => (
                    <ProductItem addToCart={this.addToCart} key={item._id} cartId={this.props.cart.front._id} data={item} />
                ))}
            </Row> : null}
            </React.Fragment>
        )

    }
}
const mapStateToProps =  ({cart}) => (
    {cart}
) 

ProductList.defaultProps = {
    title: "Produkty"
}

export default connect(mapStateToProps, { ...cartActions })(ProductList);