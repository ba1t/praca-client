import * as actions from "./actionCreators";
import reducer from "./reducers";

// // Store/state related stuff:
export default reducer;
export { actions };

//table 

export { default as ProductTable } from './table/ProductTable';  
export { default as SettingsForm } from './form/SettingsForm';
// // Components:
export { default as ProductList } from './components/ProductList.js';
// export { default as CategoryTableAdmin } from './components/Category_table_admin';
// export { default as CategoryFormAdmin } from './components/Category_form_admin';