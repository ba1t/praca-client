import * as Types from "../category/actionTypes";
import services from './services'
export const fetchCategories = () => async dispatch => {
    const res = await services.fetchCategories();
    dispatch({type: Types.FETCH_CATEGORY, payload: {list: res.data.doc}});
}
// export const getCategoryByPath = (path) => async dispatch => {
//     const res = await axios.get(`/api/category/path/${path}`);
//     dispatch({type: Types.GET_CATEGORY, payload: res.data.doc})
// }
export const getCategory = (id, cb) => async dispatch => {
    const res = await services.getCategory(id);
    dispatch({type: Types.GET_CATEGORY, payload: res.data.doc})
};
export const addCategory = (data, cb) => async dispatch => {
    const res = await services.addCategory(data);
    dispatch({type: Types.ADD_CATEGORY, payload: res.data.doc});
    cb();
}
export const updateCategory = (id,data, cb) => async dispatch => {
    const res = await  services.updateCategory(id, data);
    dispatch({type: Types.UPDATE_CATEGORY, payload: res.data.doc});
    cb();
}
export const deleteCategory = (id) => dispatch => {
    services.deleteCategory(id);
    dispatch({type: Types.DELETE_CATEGORY, payload: id})
}
export const clearCategory = () => dispatch => {
    dispatch({type: Types.CLEAR_CATEGORY, payload: {details: false, updateCategory: false, categoryDeleted: false}})
}