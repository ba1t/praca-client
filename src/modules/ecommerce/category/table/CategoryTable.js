import React from 'react';

import {Table} from '../../../../shared/Table'
import Button from '../../../../shared/Button/Button'
import ButtonGroup from '../../../../shared/ButtonsGroup/ButtonGroup';
import { FaTrash } from 'react-icons/fa';

export default (props) => {
    const columns = [
        { head: 'Waga', accessor: 'weight'},       
        { head: 'Name', accessor: 'name', width: "25%" },       
        { head: 'Parent', accessor: 'parent.name', width: "25%" },
        { head: 'Path', accessor: 'path', width: "50%" }
    ];

    function actions(arg) {
        return (
            <ButtonGroup>
                <Button to={`/admin/ecommerce/categories/${arg.id}/edit`} color="secoundary">Edytuj</Button>
                <Button color="danger" onClick={() => (props.deleteCategory(arg.id))}><FaTrash /></Button>
            </ButtonGroup>
        )
    }
    return (
        <Table columns={columns} actions={actions} rows={props.category.list} />      
    )
}
