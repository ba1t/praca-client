import * as Types from "./actionTypes";
import _ from 'lodash';

export default function(state = {}, action) {
    switch (action.type) {
        case Types.FETCH_CATEGORY:
            return { ...state, ...action.payload }
        case Types.GET_CATEGORY:
            return {...state, details: action.payload}
        case Types.ADD_CATEGORY:
            return {...state,newOrder: action.payload}
        case Types.UPDATE_CATEGORY:
            return {
                ...state,
                updateCategory: action.payload.success,
                category: action.payload.doc
            }
        case Types.DELETE_CATEGORY:
            const newState = [...state.list];
            const deleteItem = _.findIndex(newState, ['_id', action.payload]);
            newState.splice(deleteItem, 1);
            return {
                ...state,
                count: state.count - 1,
                list: newState,
                categoryDeleted:action.payload
            }
        case Types.CLEAR_CATEGORY:
            return {
                ...state,
                updateCategory: action.payload.updateCategory,
                details: action.payload.details,
                categoryDeleted: action.payload.categoryDeleted
            }
        default:
            return state;
    }
}