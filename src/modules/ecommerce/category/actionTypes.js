export const FETCH_CATEGORY = '@@store/ecoomerce/category/FETCH_CATEGORY'; 
export const GET_CATEGORY = '@@store/ecoomerce/category/GET_CATEGORY';
export const ADD_CATEGORY = '@@store/ecoomerce/category/ADD_CATEGORY';
export const UPDATE_CATEGORY = '@@store/ecoomerce/category/UPDATE_CATEGORY';
export const DELETE_CATEGORY = '@@store/ecoomerce/category/DELETE_CATEGORY';
export const CLEAR_CATEGORY = '@@store/ecoomerce/category/CLEAR_CATEGORY';