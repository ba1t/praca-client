
import React from 'react';
import {withRouter} from 'react-router-dom';
import Form from "react-jsonschema-form";
class SettingsForm extends React.Component {
    constructor(props){
        super(props);
        this.onSubmit = this.onSubmit.bind(this);
    }
    schema = {
        "type": "object",
        "required": [
            "name",
            "path"
        ],
        "properties": {
            "name": {
                "type": "string",
                "title": "Nazwa Kategorii",
            },
            "enable": {
                "type": "boolean",
                "title": "Enable",
                "default": true
            },
            "weight": {
                "title": "Waga",
                "type": "number"
            },
            "path": {
                "type": 'string',
                "title": 'Adres url',
            }
        }
    }
    onSubmit({formData}){
        this.props.edit ?
            this.props.updateCategory(this.props.match.params.id, formData, () => ( this.props.history.push("/admin/ecommerce/categories") ))
            : this.props.addCategory(formData, () => ( this.props.history.push("/admin/ecommerce/categories") ));

        }
    
    render() {
        return (
            this.props.category.list ? 
            <Form schema={this.schema} onSubmit={this.onSubmit} formData={this.props.category.details} /> : null
        )
    }
}

export default withRouter(SettingsForm);