import axios from 'axios';

export default {
    fetchCategories() {
        return axios.get('/api/v1/category')
    },
    getCategory(id) {
        return axios.get(`/api/v1/category/${id}`);
    },
    addCategory(data) {
        return axios.post(`/api/v1/category`, data);
    },
    updateCategory(id, data) {
        return axios.put(`/api/v1/category/${id}`, data);
    },
    deleteCategory(id) {
        return axios.delete(`/api/v1/category/${id}`);
    }
}