import React from 'react';
import { connect } from 'react-redux';
import { actions,  } from '../';
import {Link} from 'react-router-dom';
import {NavDropdown} from 'react-bootstrap';

class CategoryNavMenu extends React.Component   {

    componentDidMount() {
        this.props.fetchCategories();
    }
    render() {
        return(
            this.props.category.list ? 
            <>
                {this.props.category.list.map(item => {
                 return (
                    <NavDropdown.Item key={item._id}  as={Link} to={`/category/${item.path}`}>{item.name}</NavDropdown.Item>
                 )
                })}
            </> : null
        )
    }
}

const mapStateToProps =  ({category}) => (
    {category}
) 

export default connect(mapStateToProps, { ...actions })(CategoryNavMenu);


