import React from 'react';
import { connect} from 'react-redux'
import {actions} from '../../../ecommerce/cart'
import { Link } from 'react-router-dom'
import {Badge, Button} from 'react-bootstrap';
import {FaShoppingCart} from 'react-icons/fa';

class CartWidget extends React.Component {
    componentDidMount() {
        this.props.getFrontCart();
    }
    render() {
        return (
            this.props.cart.front ?
                <div className="cart_widget">
                    <div className="cart_widget__counter">
                        <Button as={Link} to={`/koszyk/${this.props.cart.front._id}`}>
                            <h4>
                                <FaShoppingCart className="shopingCart"/> 
                                <Badge variant="danger">{this.props.cart.front.products.length}</Badge>
                            </h4>
                        </Button>
                    </div>
                </div>
            : null
        )
    }
}
const mapStateToProps =  ({cart}) => (
    {cart}
) 

export default connect(mapStateToProps, { ...actions })(CartWidget);