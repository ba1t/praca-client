import React from 'react';
import axios from 'axios';
import {withRouter} from 'react-router-dom';
import Form from "react-jsonschema-form";
import { loadStripe } from '@stripe/stripe-js';
const stripePromise = loadStripe('pk_test_o1QBYnmTcSBvJgMgMUMfgOSm00TWfzKO8G');

class CartForm extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      paymentError = false
    }
    this.onSubmit = this.onSubmit.bind(this);
  }
  schema = {
        "type": "object",
        "required": [
            "firstname",
            "lastname"
        ],
        "properties": {
            "firstname": {
                "type": "string",
                "title": "Imię",
            },
            'lastname': {
                "type": "string",
                "title": "Nazwisko"
            },
            'adress': {
                "type": "string",
                "title": "address",
            },
            'phone': {
                "type": 'string',
                "title": 'phone'
            },
     
        }
    }
  async onSubmit({formData}){
    const stripe = await stripePromise;
    
    const response = await axios.post("/api/v1/confirm", this.props.data);
            
    const session = response.data;
    
    const result = await stripe.redirectToCheckout({
      sessionId: session.id,
    });
    
    if (result.error) {
      this.setState({paymentError: true})
    } else {
      const order = await axios.post("/api/v1/order", {formData, products: this.props.data});
    }
  }
    
  render() {
    return (
      <div>
        {this.state.paymentError ? <div>Problem z płatnością</div> : null}
        <Form onSubmit={this.onSubmit} schema={this.schema}/>
      </div>
    ) 
  }
}

export default withRouter(CartForm);

