import axios from 'axios';

export default {
    fetchCarts() {
        return axios.get('/api/v1/cart')
    },
    getCart(id) {
        return axios.get(`/api/v1/category/${id}`);
    },
    addToCart(id) {
        return axios.post(`/api/v1/cart/add/${id}`);
    },
    getFrontCart() {
        return axios.get(`/api/v1/getCart`);
    },
    removeFromCart(id) {
        return axios.post(`/api/v1/cart/remove/${id}`, id);
    }
}