import React from 'react';

import {Table} from '../../../../shared/Table'

export default (props) => {
    const columns = [
        { head: 'createdAt', accessor: 'createdAt'},       
        { head: 'updatedAt', accessor: 'updatedAt', width: "25%" },       
    ];

    return (
        <Table columns={columns} rows={props.cart.list} />      
    )
}
