export const FETCH_CARTS = '@@store/ecoomerce/cart/FETCH_CARTS'; 
export const GET_CART = '@@store/ecoomerce/cart/GET_CART';
export const ADD_TO_CART = '@@store/ecoomerce/cart/ADD_TO_CART';
export const REMOVE_FROM_CART = '@@store/ecoomerce/cart/REMOVE_FROM_CART';
export const CLEAR_CART = '@@store/ecoomerce/cart/CLEAR_CART';
export const GET_FRONT_CART = '@@store/ecoomerce/cart/GET_FRONT_CART';