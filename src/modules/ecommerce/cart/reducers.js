import * as Types from "./actionTypes";

export default function(state = {}, action) {
    switch (action.type) {
        case Types.FETCH_CARTS:
            return { ...state, ...action.payload }
        case Types.GET_CART:
            return {...state, details: action.payload}
        case Types.GET_FRONT_CART:
            return {...state, front: action.payload};
        case Types.ADD_TO_CART:
            return {...state, front: action.payload};
        case Types.REMOVE_FROM_CART:
            return {...state, front: action.payload};
        default:
            return state;
    }
}