import * as Types from "./actionTypes";
import services from './services'
export const fetchCarts = () => async dispatch => {
    const res = await services.fetchCarts();
    dispatch({type: Types.FETCH_CARTS, payload: {list: res.data.doc}});
};
export const getCart = (id) => async dispatch => {
    // const res = await services.getCart(id);
    // dispatch({type: Types.GET_CART, payload: })
};
export const getFrontCart = (cb = false) => async dispatch => {
    const res = await services.getFrontCart();
    dispatch({type: Types.GET_FRONT_CART, payload: res.data.doc})
    if(cb) {
        cb(res.data.doc);
    }
}
export const addToCart = (id, cb = false) => async dispatch => {
    const res = await services.addToCart(id);
    dispatch({type: Types.ADD_TO_CART, payload: res.data.doc});
    if(cb){
        cb();
    }
}
export const removeFromCart = (id) => async dispatch => {
    const res = await  services.removeFromCart(id);
    dispatch({type: Types.REMOVE_FROM_CART, payload: res.data.doc});
}
export const clearCart = (id) => dispatch => {
    services.deleteCategory(id);
    dispatch({type: Types.CLEAR_CART, payload: id})
}
