import * as Types from "./actionTypes";
import _ from 'lodash';

export default function(state = {}, action) {
    switch (action.type) {
        case Types.FETCH_PRODUCER:
            return { ...state, ...action.payload }
        case Types.GET_PRODUCER:
            return {...state, details: action.payload}
        case Types.ADD_PRODUCER:
            return {...state,newOrder: action.payload}
        case Types.UPDATE_PRODUCER:
            return {
                ...state,
                updateProducer: action.payload.success,
                producer: action.payload.doc
            }
        case Types.DELETE_PRODUCER:
            const newState = [...state.list];
            const deleteItem = _.findIndex(newState, ['_id', action.payload]);
            newState.splice(deleteItem, 1);
            return {
                ...state,
                count: state.count - 1,
                list: newState,
                producerDeleted:action.payload
            }
        case Types.CLEAR_PRODUCER:
            return {
                ...state,
                updateProducer: action.payload.updateProducer,
                details: action.payload.details,
                producerDeleted: action.payload.producerDeleted
            }
        default:
            return state;
    }
}