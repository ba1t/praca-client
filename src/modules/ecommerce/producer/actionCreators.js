import * as Types from "./actionTypes";
import services from './services'
export const fetchProducer = () => async dispatch => {
    const res = await services.fetchProducer();
    dispatch({type: Types.FETCH_PRODUCER, payload: {list: res.data.doc}});
}
export const getProducer = (id, cb) => async dispatch => {
    const res = await services.getProducer(id);
    dispatch({type: Types.GET_PRODUCER, payload: res.data.doc})
}
export const addProducer = (data, cb) => async dispatch => {
    const res = await services.addProducer(data);
    dispatch({type: Types.ADD_PRODUCER, payload: res.data.doc});
    cb();
}
export const updateProducer = (id,data, cb) => async dispatch => {
    const res = await  services.updateProducer(id, data);
    dispatch({type: Types.UPDATE_PRODUCER, payload: res.data.doc});
    cb();
}
export const deleteProducer = (id) => dispatch => {
    services.deleteProducer(id);
    dispatch({type: Types.DELETE_PRODUCER, payload: id})
}
export const clearProducer = () => dispatch => {
    dispatch({type: Types.CLEAR_PRODUCER, payload: {details: false, updateProducer: false, producerDeleted: false}})
}