import React from 'react';
import { connect } from 'react-redux';
import {actions } from '..';

import {Table} from '../../../../shared/Table'
import Button from '../../../../shared/Button/Button'
import ButtonGroup from '../../../../shared/ButtonsGroup/ButtonGroup';
import { FaTrash } from 'react-icons/fa';

class ProducerTable extends React.Component {
    constructor(props) {
        super(props);
        this.action = this.action.bind(this);
    }
    columns = [
        { head: 'Name', accessor: 'name', width: "100%" }        
    ];
    componentDidMount() {
        this.props.fetchProducer();
    }
    action(arg) {
        return (
            <ButtonGroup>
                <Button to={`/admin/ecommerce/producers/${arg.id}/edit`} color="secoundary">Edytuj</Button>
                <Button color="danger" onClick={() => (this.props.deleteProducer(arg.id))}><FaTrash /></Button>
            </ButtonGroup>
        )
    }
    render() {
        return (
            this.props.producer.list ? 
                <Table columns={this.columns} actions={this.action} rows={this.props.producer.list} /> 
                : null       
        )
    }
}

const mapStateToProps =  ({producer}) => (
    {producer}
) 

export default connect(mapStateToProps, { ...actions })(ProducerTable);