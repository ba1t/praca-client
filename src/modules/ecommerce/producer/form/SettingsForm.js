import React from 'react';
import {withRouter} from 'react-router-dom';
import Form from "react-jsonschema-form";
class SettingsForm extends React.Component {
    constructor(props){
        super(props);
        this.onSubmit = this.onSubmit.bind(this);
    }
    schema = {
        "type": "object",
        "required": [
            "name"
        ],
        "properties": {
            "name": {
                "type": "string",
                "title": "Nazwa Producent",
            }
        }
    }
    onSubmit({formData}){
        this.props.edit ?
            this.props.updateProducer(this.props.match.params.id, formData, () => ( this.props.history.push("/admin/ecommerce/producers") ))
            : this.props.addProducer(formData, () => ( this.props.history.push("/admin/ecommerce/producers") ));

        }
    
    render() {
        return (
            <Form schema={this.schema} onSubmit={this.onSubmit} formData={this.props.producer.details} />
        )
    }
}

export default withRouter(SettingsForm);