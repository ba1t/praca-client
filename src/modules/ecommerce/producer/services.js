import axios from 'axios';

export default {
    fetchProducer() {
        return axios.get('/api/v1/manufacturer')
    },
    getProducer(id) {
        return axios.get(`/api/v1/manufacturer/${id}`);
    },
    addProducer(data) {
        return axios.post(`/api/v1/manufacturer`, data);
    },
    updateProducer(id, data) {
        return axios.put(`/api/v1/manufacturer/${id}`, data);
    },
    deleteProducer(id) {
        return axios.delete(`/api/v1/manufacturer/${id}`);
    }
}