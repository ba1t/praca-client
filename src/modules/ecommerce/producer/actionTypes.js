export const FETCH_PRODUCER = '@@store/ecoomerce/producer/FETCH_PRODUCER'; 
export const GET_PRODUCER = '@@store/ecoomerce/producer/GET_PRODUCER';
export const ADD_PRODUCER = '@@store/ecoomerce/producer/ADD_PRODUCER';
export const UPDATE_PRODUCER = '@@store/ecoomerce/producer/UPDATE_PRODUCER';
export const DELETE_PRODUCER = '@@store/ecoomerce/producer/DELETE_PRODUCER';
export const CLEAR_PRODUCER = '@@store/ecoomerce/producer/CLEAR_PRODUCER';