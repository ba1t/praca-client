import * as actions from "./actionCreators";
import reducer from "./reducers";

export default reducer;
export { actions };

export { default as CustomerTable } from './table/CustomerTable';
