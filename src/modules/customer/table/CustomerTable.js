import React from 'react';

import {Table} from '../../../shared/Table'

export default (props) => {
    const columns = [
        { head: 'id', accessor: '_id'},       
        { head: 'email', accessor: 'email', width: "25%" },       
        { head: 'Imie', accessor: 'profile.firstname', width: "25%" },
        { head: 'Nazwisko', accessor: 'profile.lastname', width: "50%" }
    ];

    return (
        <Table columns={columns}  rows={props.customer.list} />      
    )
}