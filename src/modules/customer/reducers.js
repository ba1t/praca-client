import * as Types from "./actionTypes";

export default function(state = false, action) {
    switch (action.type) {
        case Types.FETCH_CUSTOMERS:
            return { ...state, ...action.payload }
        default:
            return state;
    }
}