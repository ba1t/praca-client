import * as Types from "./actionTypes";
import services from './services'

export const fetchCustomers = (cb = false) => async dispatch => {
    const res = await services.fetchCustomers();
    dispatch({type: Types.FETCH_CUSTOMERS, payload: {list: res.data.data}})
    if(cb) {
        cb();
    }
}
