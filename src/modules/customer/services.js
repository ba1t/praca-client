import axios from 'axios';

export default {
    fetchCustomers() {
        return  axios.get('/api/v1/customer')
    },
}   