import React from 'react';
import { BrowserRouter, Switch, Route, Link } from 'react-router-dom';
import { renderRoutes } from "./lib/routes";
import { Button } from 'react-bootstrap';
import adminTheme from './themes/admin'
import websiteTheme from './themes/website'
import {adminRoutes, websiteRoutes} from './config/routes';

const Page404 = () => (
    <div>
    <div>404</div>

    <Button as={Link} to={`/`}>Do strony głównej</Button>
    </div>
    
)
const Router = props => {
    return (
        <BrowserRouter>
          <Switch>
            {renderRoutes(websiteRoutes, websiteTheme, props.auth)} 
            {renderRoutes(adminRoutes, adminTheme, props.auth, '/admin')} 
            <Route component={Page404} />
          </Switch>
        </BrowserRouter>
    )
}

export default Router;