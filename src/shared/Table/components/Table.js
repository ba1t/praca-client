import React from 'react';
import _ from 'lodash';
import dot from 'dot-object';

export default (props) => {
    return (
        <table className="table">
            <thead>
                <tr>
                    <th style={{width: 20}}>Lp</th>
                    {props.columns.map(({head, width}) => (
                        <th style={{width}} key={head}>{head}</th>
                    ))}
                    {props.actions && <th className="table__action"></th>}
                </tr>
            </thead>
            <tbody>
                {props.rows.map((row, index) => (
                    <tr key={row._id + "tr"}> 
                        <th>{index + 1}</th>
                        {props.columns.map(({accessor}, index) => {
                            const acc = dot.pick(accessor, row);
                            return (
                            <td key={row._id + "td"}>
                                {_.isBoolean(acc) ? <input type="checkbox" checked={acc} /> : acc}
                            </td>)
                        })}
                        {props.actions && <td className="table__action"><props.actions id={row._id}/></td>}
                    </tr>
                ))}
            </tbody>
        </table>
    )
}


