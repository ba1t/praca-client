import React from 'react';
import './style.scss';
const ButtonGroup = (props) => (
    <div className={`btn-group btn-group--${props.orientation}`}>{props.children}</div>
)

ButtonGroup.defaultProps = {
    orientation: 'horizontal'
}

export default ButtonGroup;