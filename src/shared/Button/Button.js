import React from 'react';
import { Link } from 'react-router-dom'
import './style.scss';
const Button =  (props) => {
    function renderButton () {
        return <button onClick={props.onClick} type={props.type} className={`btn btn--${props.color} ${props.className}`}>{props.children}</button>
    }
    return (
        props.to ? 
            <Link to={props.to}>{renderButton()}</Link> 
            : <React.Fragment>{renderButton()}</React.Fragment>
    )
}

Button.defaultProps = {
    color: 'primary',
    type: 'button',
    to: false,
}

export default Button;