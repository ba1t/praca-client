import React from 'react';
import Button from '../../Button/Button';

const MultipleField = (props) => {
    const addField = (field) => {
        console.log(field);
    }
    console.log(props)
    return(
        <React.Fragment>
            {props.getType(props.label)}
            <Button className="form__multiple-add" onClick={addField.bind(this, props)}>Dodaj kolejny</Button>
        </React.Fragment>
    )
}

export default MultipleField;