import React, { Component, createContext } from 'react';
import _ from 'lodash';
import {Input, Checkbox, Select, Textarea, File, Fieldset, Collection} from '../';

import dot from 'dot-object';
import Button from '../../Button/Button';
import validator from 'validator';

export const FormContext = createContext();

class FormBuilder extends Component {
    constructor(props) {
        super(props);
        this.state = {
            exclude: ['container', 'multiple'],
            init: false,
            values: {},
            validateSchema: {},
            validateForm: false,
            errors: {}
        }
        this.setValues = this.setValues.bind(this);
        this.getType = this.getType.bind(this);
        this.validateField = this.validateField.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }
    componentDidMount() {
        let values = {};
        let validates = {};
        let errors = {};
        const mapFields = (data) => {
            console.log(this.props.data);
            data.map(element => {
                if(this.state.exclude.includes(element.type)) {
                    return mapFields(element.fields);
                } else {
                    if(_.has(this.props.data, element.name)) {
                        _.merge(values, {[element.name]: this.props.data[element.name]});
                    } else {
                        if(_.has(element, 'validate')) {
                            _.merge(validates, {[element.name]: element.validate});
                            let required = _.findIndex(element.validate, ["type", 'require'] );
                            if(~required && !_.has(element, 'default')) {
                                _.merge(values, {[element.name]: ""})
                                _.merge(errors, {[element.name]: this.defaultMessage(element.validate[required].message, `This field is required`)})
                            }
                        }
                        if(_.has(element, 'default')) {
                            _.merge(values, {[element.name]: element.default});
                        }
                        if(element.type === 'select' && !element.empty) {
                            _.merge(values, {[element.name]: element.options[0].value});
                        }
                        if(element.type === 'file') {
                            _.merge(values, {[element.name]: []});
                        }
                    }
                    return this;
                }
            });
        };
        mapFields(this.props.config)
        this.setState({values: values, validateSchema: validates, errors: errors, init: true}, () => this.state);
    }

    setValues(obj) {
        this.setState({values: {...this.state.values, ...obj}}, () => console.log(this.state));
    }

    defaultMessage(value, message) {
        return _.isUndefined(value) ? message : value
    }

    validateField(arr, cb = false) {
        let errors = {...this.state.errors};

        _.mapKeys(arr, (value, key) => {
            if(_.has(this.state.validateSchema, key)){
                const validate = this.state.validateSchema[key];
                let required = _.findIndex(validate, ["type", 'require'] );
     
                errors[key] = [];
                _.map(validate, (val) => {
                    if(!value.length && !~required ) {
                        errors[key] = [];
                        return false;
                    }
                    switch (val.type) {
                        case 'require':
                            if(validator.isEmpty(value.toString()) ) {
                                console.log('req', val)
                                return errors[key].push(this.defaultMessage(val.message, `This field is required`));
                            }
                            break;
                        case 'range':
                            if(value < val.config.min || value > val.config.max) {
                                return errors[key].push(this.defaultMessage(val.message, `lengt of text must be betwene ${val.config.min}, ${val.config.max}`));
                            }
                            break;
                        case 'email':
                            if(!validator.isEmail(value)) {
                                return errors[key].push(this.defaultMessage(val.message, `field must be a email`));
                            }
                            break
                        default:
                            return errors[key];
                    }
                })
                if(!errors[key].length) {
                    errors[key] = false;
                }
            }
            return errors[key];
        })
        if(cb)
            cb(null, arr)
        this.setState({errors: errors});

    }; 
    getType(element) {
        // if(element.multiple) { 
        //     const ele = {...element, multiple: false}
        //     return <MultipleField  {...ele} getType={this.getType} />
        // } else {
            switch(element.type) {
                case 'input': 
                    return <Input {...element} />;
                case 'checkbox':
                    return <Checkbox {...element} />;
                case 'select':
                    return <Select {...element} />;
                case 'textarea':
                    return <Textarea {...element} />;
                case 'file':
                    return <File {...element} />;
                case 'fieldset':
                    return <Fieldset {...element} getType={this.getType} />;
                case 'collection':
                    return <Collection {...element} getType={this.getType} />;
                case 'container':
                    return <div className="form__container">{element.fields.map(field => (
                        this.getType(field)
                    ))}
                    </div>;
                default:
                    return <Input {...element}/>;
            }
        // }
    }
    onSubmit(e){
        e.preventDefault();
        if(!this.state.validateForm) {
            this.setState({validateForm: true})
        }

        this.validateField(this.state.values);

        if(_.values(this.state.errors).every(err => err === false)) {
            console.log('xxx');
            this.props.onSubmit.bind(this, dot.object({...this.state.values}))();
        }
    }
    
    render() {
        return(
            this.state.init ?
                <FormContext.Provider value={
                    {
                        form: this.state, 
                        validateField: this.validateField,
                        setValues: this.setValues
                    }
                }>
                    <form className="form" onSubmit={this.onSubmit}>
                        {this.props.config.map(element => {
                            return (
                                <React.Fragment key={element.name}>
                                    {this.getType(element)}
                                </React.Fragment>
                            )
                        })}
                        <Button type="submit" className="form__submit">{this.props.edit ? "zapisz zmiany" : "Dodaj nowy"}</Button>
                    </form>
                </FormContext.Provider>
            :null
        )
    }

};

export default FormBuilder;

