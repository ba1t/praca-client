import React, {useContext, useState} from 'react';
import {FormContext} from '../Form';
import axios from "axios";
import _ from "lodash";
const File = (props) => {
    const context = useContext(FormContext);
    const [value, setValue] = useState(context.form.values[props.name]);
    const required = ~_.findIndex(props.validate, ["type", 'require'] );
    const onChange = (setValues, validateField, e) => {
        const formData = new FormData();
        formData.append('images', e.target.files[0])
        e.target.value = '';
        
        const config = {
            headers: {
                'content-type': 'multipart/form-data'
            }
        };
        axios.post('/api/upload', formData, config).then(res => {
            setValue(res.data[0])
            validateField({[props.name]: res.data[0]}, (err, arr) => {
                if(!err) {
                    setValues(arr)
                }
            })
        })
    };

    return (
        
        <div className="form__file">       
            {props.label && <label>{!!required && <span>*</span>}{props.label}</label>}            
            {!!value  ?
                <img alt='' tile="" style={{width: '220px'}} src={`/${value.path}`} />
             : <input multiple type="file" accept="image/png, image/jpeg" onChange={onChange.bind(this, context.setValues, context.validateField)} /> }
        </div>
    )
};

export default File;