import React, { useState, useContext } from 'react';
import {FormContext} from '../Form';
import _ from 'lodash';

import Button from '../../../Button/Button'
import ButtonGroup from '../../../ButtonsGroup/ButtonGroup';

const Collection = (props) => {
    const context = useContext(FormContext);
    const [values, setValues] = useState(context.form.values[props.name] ? context.form.values[props.name] : []);

    const addField = (field) => {
        console.log(field);
        setValues([...values, field])
    }
    return(
        <React.Fragment>
            <div className="form__fields">
                {_.map(values, (field, index) => (
                    props.getType({...field, name: `${props.name}.${index}.${field.name}`})
                ))}
            </div>
            <ButtonGroup>
                {_.map(props.fields, (field) => (
                    <Button onClick={addField.bind(this, field)}>{field.label}</Button>
                ))}
            </ButtonGroup>
        </React.Fragment>

        
    )
}

export default Collection;