import React, {useContext, useState} from 'react';
import {FormContext} from '../Form';

const Checkbox = (props) => {
    const context = useContext(FormContext);
    const [value, setValue] = useState(context.form.values[props.name] );
    const onChange = (setValues, validateField, e) => {
        setValue(e.target.checked);
        if(context.form.validateForm) {
            validateField({[props.name]: value}, (err, arr) => {
                if(!err) {
                    setValues(arr)
                }
            })
        }
    }
    return (
        <div className="form__checkbox">
            <label>
                <input type="checkbox" defaultChecked={value} onChange={onChange.bind(this, context.setValues, context.validateField)} value={value} />
                {props.label && <span>{props.label}</span>}
            </label>
        </div>
    )
};


export default Checkbox;