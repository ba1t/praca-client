import React from 'react';
const Fieldset = (props) => {
    return (
        <fieldset className="form__fieldset">
            {props.label && <legend>{props.label}</legend>}
            {props.fields.map(element => {
                return (
                    <React.Fragment key={'xx'}>
                        {props.getType({...element, name: `${props.name}.${element.name}`})}
                    </React.Fragment>
                )
            })}
        </fieldset>
    )
};
export default Fieldset;