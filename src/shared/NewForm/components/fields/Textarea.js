import React, {useContext, useState} from 'react';
import {FormContext} from '../Form';
import Message from '../Message';
import _ from 'lodash';

const Textarea = (props) => {
    const context = useContext(FormContext);
    const [value, setValue] = useState(context.form.values[props.name]);
    const required = ~_.findIndex(props.validate, ["type", 'require'] );
    const onChange = (setValues, validateField, e) => {
        setValue(e.target.value);
        if(context.form.validateForm) {
            validateField({[props.name]: value}, (err, arr) => {
                if(!err) {
                    setValues(arr)
                }
            })
        }
    }
    const onBlur = (setValues, validateField, e) => {
        validateField({[props.name]: e.target.value}, (err, arr) => {
            if(!err) {
                setValues(arr)
            }
        })
    }
    return (
        <div className="form__textarea">
            {props.label && <label>{!!required && <span>*</span>}{props.label}</label>}
            <textarea  
                onChange={onChange.bind(this, context.setValues, context.validateField)}
                onBlur={onBlur.bind(this, context.setValues, context.validateField)}
            >{value}</textarea> 
            <Message name={props.name} />
        </div>
    )
};

export default Textarea;