import React, {useContext} from 'react';
import {FormContext} from './Form';
export default (props) => {
    const context = useContext(FormContext); 
    function renderErrors() {
            if(context.form.errors[props.name]){
        return (

                context.form.errors[props.name].map(ele => (
                    <div>{ele}</div>
                ))
        )
            }
    }
    return (
        (context.form.validateForm && !!context.form.errors[props.name]) ? <div>{renderErrors()}</div> : null 
    )
}