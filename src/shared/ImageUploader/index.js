import React from 'react';
import ImageUploader from 'react-images-upload';
import {fileUpload} from '../../lib/fileUpload';
export class IU extends React.Component {
 
    constructor(props) {
        super(props);
         this.state = { pictures: false };
         this.onDrop = this.onDrop.bind(this);
    }
    
    componentDidMount() {
        console.log('images', this.props.value);

        if(this.props.value)
            this.setState({pictures: {target: this.props.value._id, path: this.props.value.path}}, () => this.props.onChange(this.props.value._id));
    }
    onDrop(picture) {
        fileUpload(picture, (data) => {
            const target = data.doc[0]._id;
            const item = {target, path: data.doc[0].path}
            this.setState({
                pictures: item
            }, () => this.props.onChange(target)
        )})
    }
 
    render() {
        console.log('state', this.state);
        return (
                !this.state.pictures ?

                            <ImageUploader
                            withIcon={true}
                            buttonText='Choose images'
                            onChange={this.onDrop}
                            singleImage={true}
                            imgExtension={['.jpg', '.gif', '.png', '.gif']}
                            maxFileSize={5242880}
                        /> :
                    <img alt="" title="" style={{width: "150px", height: "150px"}} src={`/${this.state.pictures.path}`} />
                        
        );
    }
}



export const schema = (props) => {
    console.log(props);
    return (
        <IU required={props.required} {...props} />
    )
}



