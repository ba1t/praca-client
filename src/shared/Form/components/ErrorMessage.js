import React from 'react';


const ErrorMessage = (props) => {
    return (
        props.validate.map(m => (
            <div>{m.message}</div>
        ))
    )
} 

export default ErrorMessage;