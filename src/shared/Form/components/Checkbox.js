import React, {useContext} from 'react';
import {FormContext} from './Form';
const Checkbox = (props) => {
    const context = useContext(FormContext);
    const onChange = (setFormValue, e) => {
        setFormValue(props.name, e.target.checked)
    };
    return (
        <div className="form__checkbox">
            <label>
                <input type="checkbox" defaultChecked={context.values[props.name]} onChange={onChange.bind(this, context.setFormValue)} value={context.values[props.name]} />
                {props.label && <span>{props.label}</span>}
            </label>
        </div>
    )

};

export default Checkbox;