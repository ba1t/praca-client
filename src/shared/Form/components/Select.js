import React, {useContext} from 'react';
import {FormContext} from './Form';
const Select = (props) => {
    const context = useContext(FormContext);
    const onChange = (setFormValue, e) => {
        setFormValue(props.name, e.target.value)
    };
    return (
        <div className="form__select">
            {props.label && <label>{props.label}</label>}
            <select value={context.values[props.name]} onChange={onChange.bind(this, context.setFormValue)}>
                {/* {props.empty && <option key={'empty'} value={""}>-- brak --</option>} */}
                {props.options.map(option => (
                    <option key={option.value} value={option.value}>{option.name}</option>
                ))}
            </select>
        </div>
    )
};

export default Select;