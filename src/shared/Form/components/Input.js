import React, {useContext} from 'react';
import {FormContext} from './Form';
import ErrorMessage from './ErrorMessage';
import _ from 'lodash';
const Input = (props) => {
    const require = !_.findIndex(props.validate, {type: 'require'});
    const context = useContext(FormContext);

    const error = !context.formValidate && !context.validate[props.name] && _.has(props,"validate");

    const setValidate = (value) => (
        context.setValidate({...context.validate, ...{[props.name]: context.checkValidate(value, props.validate)}})
    )

    const onChange = (setFormValue, e) => {
        const value = e.target.value
        setFormValue(props.name, value)
        if(!context.formValidate && _.has(props, "validate")) {
            setValidate(value)
        }
    };
    const onBlur = (e) => {
        const value = e.target.value
        if(_.has(props, "validate")) {
            setValidate(value)
        }       
    }
    return (
        <div className="form__text">
            {props.label && <label>{require && <span>*</span>}{props.label}</label>}
            <input type="text" 
                className={error && 'form__text--error'} 
                onBlur={onBlur} 
                placeholder={props.placeholder} 
                defaultValue={context.values[props.name]} 
                onChange={onChange.bind(this, context.setFormValue)} />
            {error ? <ErrorMessage validate={props.validate} /> : null}
        </div>
    )
};

export default Input;