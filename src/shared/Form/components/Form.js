import React, {createContext, useState, useEffect} from 'react';
import {Input, Select, Textarea, Checkbox, File} from '../';
import Button from '../../Button/Button';
import md5 from 'md5';
import validator from 'validator';
import _ from 'lodash';

import dot from 'dot-object';

export const FormContext = createContext();

export const FormConsumer = FormContext.Consumer;

const exclude = ['container'];

const Form = (props) => {
    const [values, setValues] = useState(false);
    const [validate, setValidate] = useState(false);
    const [formValidate, setFormValidate ] = useState(true);
    useEffect(() => {
        let defaultVal = {};
        let valField = {};
        if(props.edit) {
            const dotData = dot.dot(props.data);
            setValues(dotData);
            setValidate(_.mapValues(dotData, v => ( true)))
        } else  {
            const mapFields = (data) => {
                data.map(element => {
                    let require = ~_.findIndex(element.validate, {type: 'require'});

                    if(exclude.includes(element.type)) {
                        return mapFields(element.fields);
                    } else {
                        _.merge(valField, {[element.name]: require && !element.default ? false : true});
                        _.merge(defaultVal, {[element.name]: element.default || null});
                        return this;
                    }
                });
            };
            mapFields(props.config);
            setValues(defaultVal);
            setValidate(valField);

        }
    }, [props.config, props.edit, props.data]);

    function setFormValue(name, obj) {
        setValues({...values, ...{[name]: obj}});
    }
    function setMultipleFormValue(obj) {
        setValues({...values, ...obj});
    }
    function checkValidate(value, array) {
        let truth = [];
        array.map(e => {
            let error;
            switch(e.type) {
                case 'email':
                    error = validator.isEmail(value);
                    break;
                case 'require':
                    error = !validator.isEmpty(value);
                    break;
                default:
                    break;
            }
            return truth.push(error)
        });

        return !truth.includes(false)

    }
    console.log(values);
    function getType(element) {
        switch(element.type) {
            case 'input': 
                return <Input {...element} />;
            case 'checkbox':
                return <Checkbox {...element} />;
            case 'select':
                return <Select {...element} />;
            case 'textarea':
                return <Textarea {...element} />;
            case 'file':
                return <File {...element} />;
            case 'container':
                return <div className="form__container">{element.fields.map(field => (
                    getType(field)
                ))}
                </div>;
            default:
                return <Input {...element}/>;
        }
    }

    const valudateForm = (e) => {
        e.preventDefault()
        setFormValidate(false);
        alert('validatae')
    };
    
    return (
        
        values ?
            <FormContext.Provider value={{values, setFormValue, setMultipleFormValue, validate, setValidate, checkValidate, formValidate}}>
                <form className="form" onSubmit={_.values(validate).includes(false) ? valudateForm : props.onSubmit.bind(this, dot.object({...values}))}>
                    {props.config.map(element => {
                        return (
                            <React.Fragment key={md5(element)}>
                                {getType(element)}
                            </React.Fragment>
                        )
                    })}
                    <Button type="submit" className="form__submit">{props.edit ? "zapisz zmiany" : "Dodaj nowy"}</Button>
                </form>
            </FormContext.Provider>
        : null
    )
};

Form.defaultsProps = {
    edit: false,
};


export default Form;