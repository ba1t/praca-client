import React, {useState} from 'react';
import {FormConsumer} from './Form';
const Textarea = ({label, placeholder, name }) => {
    const [value, setValue] = useState()
    const onChange = (setFormValue, name, e) => {
        setValue(e.target.value, setFormValue(name, e.target.value))
    } 
    return (
        <FormConsumer>
            {({setFormValue}) => {
                return (
                    <div className="form__textarea">
                        {label && <label>{label}</label>}

                        <textarea name={name}  placeholder={placeholder} onChange={onChange.bind(this, setFormValue, name)}>{value}</textarea>
                    </div>
                )
            }}
        

        </FormConsumer>
    )
}

export default Textarea;