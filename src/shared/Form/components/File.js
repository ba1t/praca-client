import React, {useContext, useState} from 'react';
import {FormContext} from './Form';
import {Input} from '../';
import axios from "axios";
import _ from "lodash";

const File = (props) => {
    const context = useContext(FormContext);
    const [files, setFiles] = useState([]);
    const onChange = (setFormValue, e) => {
        const formData = new FormData();
        _.forEach(e.target.files, file => (
            formData.append('images', file)
        ))
        e.target.value = '';
        
        const config = {
            headers: {
                'content-type': 'multipart/form-data'
            }
        };
        axios.post('/api/upload', formData, config).then(res => {
            console.log(files)
            const data  = [...files, ...res.data];
            setFiles(data)
            setFormValue(props.name, data)
        })
    };
    console.log(files);

    return (
        
        <div className="form__file">
            {!!files  ?
                _.map(files, (img,i) => {
                    return (
                        <React.Fragment>
                            <img style={{width: '220px'}} alt="" title="" src={`/${img.path}`} />
                            { props.attributes.map(att => (
                                <Input {...{...att, name: att.name.replace(/(\[).+?(\])/g, `[${i}]`)}}  />
                            ))}
                        </React.Fragment>
                    )
                }
                    
            ) : null }
            {props.label && <label>{props.label}</label>}
            <input multiple type="file" accept="image/png, image/jpeg" onChange={onChange.bind(this, context.setFormValue)} />

        </div>
    )
};

export default File;