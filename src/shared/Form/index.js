import './style.scss';
import Form from './components/Form';


export { default as Input } from './components/Input';
export { default as Checkbox } from './components/Checkbox';
export { default as Select } from './components/Select';
export { default as Textarea } from './components/Textarea';
export { default as File } from './components/File';
export { default as ErrorMessage } from './components/ErrorMessage';

export default Form;