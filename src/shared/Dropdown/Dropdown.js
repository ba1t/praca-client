import React, {useState} from 'react';

const Dropdown = (props) => {
    const [open, setOpen] = useState(false);
    return (
        <div className="dropdnown">
            <div onClick={() => setOpen(!open)} className="dropdown__header">{props.component}</div>
            { open && 
                <div className="dropdown__body">
                    {props.options}
                </div>
            }
        </div>
    )
}

export default Dropdown;