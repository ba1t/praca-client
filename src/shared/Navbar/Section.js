import React from 'react';
import ClassNames from 'classnames';
const Section = (props) => {
    const classVariables = ClassNames([
        "navbar__section",
        props.className
    ])
    return (
        <div className={classVariables}>{props.children}</div>
    )
}

export default Section;