import React from 'react';
import './style.scss';
import classNames from 'classnames';

const Navbar = (props) => {
	const classValue = classNames(
		'navbar',
		props.className,
		props.justyfi
	)
	return (
		<div className={classValue}>
			{props.children}
		</div>
	)
}
Navbar.defaultProps = {
        className: ''
}

export default Navbar;