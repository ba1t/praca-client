import { lazy } from 'react';

export const adminRoutes = [
  {
    path: "/",
    exact: true,
    component: lazy(() => import('../pages/admin/Dashboard')),
    private: true
  },
  {
    path: '/ecommerce/products',
    exact: true,
    component: lazy(() => import('../pages/admin/ecommerce/product/ProductList'))
  },
  {
    exact: true,
    component: lazy(() => import('../pages/admin/ecommerce/product/CreateProduct')),
    path: '/ecommerce/products/add'
  },
  {
    exact: true,
    component: lazy(() => import('../pages/admin/ecommerce/product/EditProduct')),
    path: '/ecommerce/products/:id/edit'
  },
  {
      path: '/ecommerce/categories',
      exact: true,
      component: lazy(() => import('../pages/admin/ecommerce/category/CategoryList'))
  },
  {
    path: '/ecommerce/categories/add',
    private: true,
    exact: true,
    component: lazy(() => import('../pages/admin/ecommerce/category/CreateCategory'))
  },
  {
    path: '/ecommerce/categories/:id/edit',
    exact: true,
    component: lazy(() => import('../pages/admin/ecommerce/category/EditCategory')),
  },
  {
    path: '/ecommerce/producers',
    exact: true,
    component: lazy(() => import('../pages/admin/ecommerce/producer/ProducerList')),
},
{
  path: '/ecommerce/producers/add',
  exact: true,
  component: lazy(() => import('../pages/admin/ecommerce/producer/CreateProducer')),
},
{
  path: '/ecommerce/producers/:id/edit',
  exact: true,
  component: lazy(() => import('../pages/admin/ecommerce/producer/EditProducer')),
},

  {
      exact: true,
      component: lazy(() => import('../pages/admin/sale/OrderList')),
      path: '/ecommerce/orders'
  },
  {
    exact: true,
    component: lazy(() => import('../pages/admin/sale/BasketList')),
      path: '/ecommerce/baskets'
  },
  {
    exact: true,
    component: lazy(() => import('../pages/admin/clients/ClientList')),
    path: '/ecommerce/customers'
  },
  {
    exact: true,
    component: lazy(() => import('../pages/admin/Raports')),
    path: '/ecommerce/raports'
  },
  {
    exact: true,
    component: lazy(() => import('../pages/admin/ecommerce/product/ProductList')),
    path: '/config/ecommerce'
  },
  {
    exact: true,
    component: lazy(() => import('../pages/admin/tax/TaxList')),
    path: '/config/tax'
  },
  {
    exact: true,
    component: lazy(() => import('../pages/admin/tax/CreateTax')),
    path: '/config/tax/add'
  },  
  {
    exact: true,
    component: lazy(() => import('../pages/admin/tax/EditTax')),
    path: '/config/tax/:id/edit'
  },

  {
    exact: true,
    component: lazy(() => import('../pages/admin/status/StatusList')),
    path: '/config/status'
  },
  {
    exact: true,
    component: lazy(() => import('../pages/admin/status/CreateStatus')),
    path: '/config/status/add'
  },  
  {
    exact: true,
    component: lazy(() => import('../pages/admin/status/EditStatus')),
    path: '/config/status/:id/edit'
  }
];

export const websiteRoutes = [
  {
    exact: true,
    component: lazy(() => import('../pages/admin/Login')),
    path: '/login'
},
{
  exact: true,  
  component: lazy(() => import('../pages/admin/Register')),
  path: '/register'
},
{
  exact: true,  
  component: lazy(() => import('../pages/admin/Profile')),
  path: '/profile'
},
  {
    exact: true,
    component: lazy(() => import('../pages/website/Home')),
    path: '/'
  },
  {
    exact: true,
    component: lazy(() => import('../pages/website/Products')),
    path: '/products'
  },
  {
    exact: true,
    component: lazy(() => import('../pages/website/ProductDetail')),
    path: '/products/:id'
  },
  {
    exact: true,
    component: lazy(() => import('../pages/website/Contact')),
    path: '/contact'
  },
  {
    exact: true,
    component: lazy(() => import('../pages/website/Cart')),
    path: '/koszyk/:id'
  },
  {
    exact: true,
    component: lazy(() => import('../pages/website/AboutUs')),
    path: '/about-us'
  },
  {
    exact: true,
    component: lazy(() => import('../pages/website/Category')),
    path: '/category/:path'
  },
  {
    exact: true,
    component: lazy(() => import('../pages/website/Success')),
    path: '/success-payment/:id'
  },
  {
    exact: true,
    component: lazy(() => import('../pages/website/Cansel')),
    path: '/cancel-payment'
  }
]