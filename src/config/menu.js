import { FaDiscourse, FaShoppingBasket, FaSitemap, FaUsers, FaTools, FaShoppingBag } from 'react-icons/fa'
export const adminMenu = [
    {
        id: "0",
        label: 'Dashboard',
        icon: FaDiscourse,
        href: '/admin/',
    },
    {
        id: "1",
        label: 'Asortyment',
        section: 'ecommerce',
        icon: FaSitemap,
        href: '#',
        submenu: [
            {
                id: "1.1",
                label: 'Produkty',
                href: '/admin/ecommerce/products'
            },
            {
                id: "1.2",
                label: 'Kategorie',
                href: '/admin/ecommerce/categories',
            },
            {
                id: "1.2",
                label: 'Producenci',
                href: '/admin/ecommerce/producers',
            }
        ]
    },
    {
        id: "2",
        label: 'Sprzedaż',
        href: '#',
        icon: FaShoppingBasket,
        submenu: [
            {
                id: "2.1",
                label: 'Zamówienia',
                href: '/admin/ecommerce/orders'
            },
            {
                id: "2.2",
                label: 'Koszyki',
                href: '/admin/ecommerce/baskets'
            }
        ]
    },
    {
        id: "3",
        label: 'Klienci',
        icon: FaUsers, 
        href: '/admin/ecommerce/customers'
    },
    {
        id: "5",
        label: 'Ustawienia podstawowe',
        section: "Konfiguracja",
        icon: FaTools, 
        submenu: [
            {
                id: "6.1",
                label: 'Site information',
                href: '/admin/config/base',
            }
        ]
    },
    {
        id: "6",
        label: 'E-commerce',
        icon: FaShoppingBag, 
        submenu: [
            {
                id: "6.1",
                label: 'Podatek',
                href: '/admin/config/tax'
            }
        ]
    }
]